package numbull.com.zjmax;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.List;

import numbull.com.zjmax.api.ZJClient;
import numbull.com.zjmax.callback.ResponseCallback;
import numbull.com.zjmax.httpcore.RequestParams;
import numbull.com.zjmax.modules.GetRecordmodule;
import numbull.com.zjmax.modules.Recordmodule;


public class RecordActivity extends ActionBarActivity {
    private ZJApplication app;
    private Context context;
    private View headerview;
    private TextView count;
    private ListView listview;
    private RecordAdapter adapter;
    private int offset = 0;
    private int limit  = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_record);

        app = (ZJApplication)getApplication();
        context = this;

        listview = (ListView)findViewById(R.id.act_record_listview);
        count = (TextView)findViewById(R.id.act_record_count);

        findViewById(R.id.act_record_back).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                RecordActivity.this.finish();
            }

        });

        listview.setOnScrollListener(new AbsListView.OnScrollListener(){

            @Override
            public void onScrollStateChanged(AbsListView view,
                                             int scrollState) {
                // TODO Auto-generated method stub
                switch(scrollState){
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        if(view.getLastVisiblePosition() > (view.getCount() - 2)){
                            LoadData(offset);
                        }
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view,
                                 int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
                // TODO Auto-generated method stub

            }

        });

        LoadData(offset);
    }

    public void LoadData(int off){
        RequestParams params = new RequestParams();
        params.put("a", "116");
        params.put("u", app.getUserinfo().getU());
        params.put("offset", off + "");
        params.put("limit", limit + "");
        ZJClient.GetRecords(new ResponseCallback<GetRecordmodule>() {
            private static final String TAG = "ResponseCallback";

            public void onSuccess(final GetRecordmodule response) {
                Log.i(TAG, "onSuccess with object returned");
                if ("0".equals(response.getE())) {
                    if (offset == 0) {
                        count.setText(response.getTotal() + "");
                        headerview = View.inflate(RecordActivity.this, R.layout.header_my_record, null);
                        TextView tv = (TextView)headerview.findViewById(R.id.header_my_record_total);
                        if(app != null){
                            DecimalFormat df  = new DecimalFormat("###.00");
                            tv.setText((df.format(Integer.parseInt(app.getUserinfo().getTotal()) * 0.01)) + "");
                            listview.addHeaderView(headerview);
                        }
                        adapter = new RecordAdapter(context, 0, response.getList());
                        listview.setAdapter(adapter);
                        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                if(position < 1){

                                }else{
                                    Intent intent = new Intent(RecordActivity.this, RecordDetailActivity.class);
                                    intent.putExtra("id", response.getList().get(position - 1).getId());
                                    intent.putExtra("project", response.getList().get(position - 1).getName());
                                    intent.putExtra("data", response.getList().get(position - 1).getDateline());
                                    intent.putExtra("sum", response.getList().get(position - 1).getAmount());
                                    intent.putExtra("have_sum", response.getList().get(position - 1).getHold());
                                    intent.putExtra("rate", response.getList().get(position - 1).getRate());
                                    intent.putExtra("already", response.getList().get(position - 1).getDearn());
                                    intent.putExtra("will", response.getList().get(position - 1).getTearn());
                                    intent.putExtra("next", response.getList().get(position - 1).getNearn());
                                    intent.putExtra("contract", response.getList().get(position - 1).getContract());
                                    intent.putExtra("status", response.getList().get(position - 1).getStatus());
                                    intent.putExtra("request_no", response.getList().get(position - 1).getRequest_no());
                                    RecordActivity.this.startActivity(intent);
                                }
                            }
                        });
                    } else if (offset == Integer.parseInt(response.getTotal())) {
                        Toast.makeText(RecordActivity.this, "已经到底了", Toast.LENGTH_SHORT).show();
                    } else {
                        for (int i = 0; i < response.getList().size(); i++) {
                            adapter.add(response.getList().get(i));
                        }
                        adapter.notifyDataSetChanged();
                    }
                    offset = offset + response.getList().size();
                } else {
                    Toast.makeText(RecordActivity.this, "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
                }
            }

            public void onSuccess(List<GetRecordmodule> response) {
                Log.i(TAG, "onSuccess with object list returned: " + response.size());

            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                Toast.makeText(RecordActivity.this, "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                Toast.makeText(RecordActivity.this, "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
            }

            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                Toast.makeText(RecordActivity.this, "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
            }
        }, params);
    }

    class RecordAdapter extends ArrayAdapter<Recordmodule> {
        private Context context;
        private List<Recordmodule> list;

        public RecordAdapter(Context context, int resource,
                             List<Recordmodule> list) {
            super(context, resource, list);
            // TODO Auto-generated constructor stub
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return list.size();
        }

        @Override
        public int getPosition(Recordmodule item) {
            // TODO Auto-generated method stub
            return super.getPosition(item);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return super.getItemId(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            ViewHolder holder = null;
            if(convertView == null){
                holder = new ViewHolder();
                convertView = View.inflate(context, R.layout.item_record_listview, null);

                holder.num = (TextView)convertView.findViewById(R.id.item_record_listview_num);
                holder.date = (TextView)convertView.findViewById(R.id.item_record_listview_date);
                holder.name = (TextView)convertView.findViewById(R.id.item_record_listview_name);
                holder.amount = (TextView)convertView.findViewById(R.id.item_record_listview_amount);
			    holder.status = (TextView)convertView.findViewById(R.id.item_record_listview_status);

                convertView.setTag(holder);
            }else{
                holder = (ViewHolder)convertView.getTag();
            }

            holder.num.setText("电子编号" + list.get(position).getInvoice());
            holder.date.setText(list.get(position).getDateline());
            holder.name.setText(list.get(position).getName());
            DecimalFormat df  = new DecimalFormat("###.00");
            holder.amount.setText((df.format(Integer.parseInt(list.get(position).getAmount()) * 0.01)) + "");
            String s = list.get(position).getStatus();
            if("0".equals(s)){
                holder.status.setText("持有中");
            }else if("1".equals(s)){
                holder.status.setText("未支付");
            }else if("2".equals(s)){
                holder.status.setText("转让中");
            }else if("3".equals(s)){
                holder.status.setText("已转让");
            }else if("3".equals(s)){
                holder.status.setText("已结算");
            }

            return convertView;
        }

        class ViewHolder {
            private TextView num;
            private TextView date;
            private TextView name;
            private TextView amount;
            private TextView status;
        }
    }

}
