package numbull.com.zjmax;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;


public class SetActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_set);

        findViewById(R.id.act_set_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetActivity.this.finish();
            }
        });
        findViewById(R.id.act_set_real_name).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ZJApplication app = (ZJApplication)getApplication();
                if(!"1".equals(app.getUserinfo().getReal_verified())){

                }else{
                    SetActivity.this.startActivity(new Intent(SetActivity.this, DoRealnameActivity.class));
                }
            }
        });
    }

}
