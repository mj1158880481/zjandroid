package numbull.com.zjmax;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;


public class BranchActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_branch);

        findViewById(R.id.act_branch_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BranchActivity.this.finish();
            }
        });
    }

}
