package numbull.com.zjmax;

import android.support.v7.app.ActionBarActivity;
import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import numbull.com.zjmax.fragments.NewsFragmentf;
import numbull.com.zjmax.fragments.NewsFragmentfo;
import numbull.com.zjmax.fragments.NewsFragments;
import numbull.com.zjmax.fragments.NewsFragmentt;


public class NewsActivity extends ActionBarActivity implements OnClickListener{
    private ViewPager viewpager;
    private TextView tvpingtai, tvchanping, tvmeiti, tvhangye;
    private View tagpingtai, tagchanping, tagmeiti, taghangye;
    private int COLOR_SELE, COLOR_UNSELE, COLOR_TAG;
    private List<Fragment> fragmentlist;
    private NewsFragmentf f1 = null;
    private NewsFragments f2 = null;
    private NewsFragmentt f3 = null;
    private NewsFragmentfo f4 = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_news);

        findViewById(R.id.act_news_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewsActivity.this.finish();
            }
        });

        tvpingtai = (TextView)findViewById(R.id.act_news_id_pingtai_tv);
        tvchanping = (TextView)findViewById(R.id.act_news_id_chanping_tv);
        tvmeiti = (TextView)findViewById(R.id.act_news_id_meiti_tv);
        tvhangye = (TextView)findViewById(R.id.act_news_id_hangye_tv);
        tagpingtai = (View)findViewById(R.id.act_news_id_pingtai_tag);
        tagchanping = (View)findViewById(R.id.act_news_id_chanping_tag);
        tagmeiti = (View)findViewById(R.id.act_news_id_meiti_tag);
        taghangye = (View)findViewById(R.id.act_news_id_hangye_tag);

        COLOR_SELE = Color.parseColor("#323232");
        COLOR_UNSELE = Color.parseColor("#929292");
        COLOR_TAG = Color.parseColor("#f14338");

        findViewById(R.id.act_news_back).setOnClickListener(new OnClickListener(){

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                NewsActivity.this.finish();
            }

        });
        findViewById(R.id.act_news_id_pingtai).setOnClickListener(this);
        findViewById(R.id.act_news_id_chanping).setOnClickListener(this);
        findViewById(R.id.act_news_id_meiti).setOnClickListener(this);
        findViewById(R.id.act_news_id_hangye).setOnClickListener(this);

        viewpager = (ViewPager)findViewById(R.id.act_news_viewpager);

        fragmentlist = new ArrayList<Fragment>();
        fragmentlist.add(f1);
        fragmentlist.add(f2);
        fragmentlist.add(f3);
        fragmentlist.add(f4);
        viewpager.setAdapter(new Adapter(getSupportFragmentManager(), fragmentlist));
        viewpager.setOnPageChangeListener(new OnPageChangeListener(){

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                switch(arg0){
                    case 0:
                        changetab(0);
                        break;

                    case 1:
                        changetab(1);
                        break;

                    case 2:
                        changetab(2);
                        break;

                    case 3:
                        changetab(3);
                        break;
                }
            }

        });
    }

    private class Adapter extends FragmentPagerAdapter {
        private List<Fragment> fragmentlist;

        public Adapter(FragmentManager fm, List<Fragment> fragmentlist) {
            super(fm);
            // TODO Auto-generated constructor stub
            this.fragmentlist = fragmentlist;
        }

        @Override
        public Fragment getItem(int arg0) {
            // TODO Auto-generated method stub
            if(arg0 == 0){
                if(f1 == null)
                    f1 = NewsFragmentf.newInstance();

                return f1;
            }else if(arg0 == 1){
                if(f2 == null)
                    f2 = NewsFragments.newInstance();

                return f2;

            }else if(arg0 == 2){
                if(f3 == null)
                    f3 = NewsFragmentt.newInstance();

                return f3;

            }else{
                if(f4 == null)
                    f4 = NewsFragmentfo.newInstance();

                return f4;
            }

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return fragmentlist.size();
        }

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch(v.getId()){
            case R.id.act_news_id_pingtai:
                changetab(0);
                viewpager.setCurrentItem(0);
                break;

            case R.id.act_news_id_chanping:
                changetab(1);
                viewpager.setCurrentItem(1);
                break;

            case R.id.act_news_id_meiti:
                changetab(2);
                viewpager.setCurrentItem(2);
                break;

            case R.id.act_news_id_hangye:
                changetab(3);
                viewpager.setCurrentItem(3);
                break;
        }
    }

    private void changetab(int poi){
        switch(poi){
            case 0:
                tvpingtai.setTextColor(COLOR_SELE);
                tvchanping.setTextColor(COLOR_UNSELE);
                tvmeiti.setTextColor(COLOR_UNSELE);
                tvhangye.setTextColor(COLOR_UNSELE);
                tagpingtai.setBackgroundColor(COLOR_TAG);
                tagchanping.setBackgroundColor(Color.WHITE);
                tagmeiti.setBackgroundColor(Color.WHITE);
                taghangye.setBackgroundColor(Color.WHITE);
                viewpager.setCurrentItem(0);
                break;

            case 1:
                tvpingtai.setTextColor(COLOR_UNSELE);
                tvchanping.setTextColor(COLOR_SELE);
                tvmeiti.setTextColor(COLOR_UNSELE);
                tvhangye.setTextColor(COLOR_UNSELE);
                tagpingtai.setBackgroundColor(Color.WHITE);
                tagchanping.setBackgroundColor(COLOR_TAG);
                tagmeiti.setBackgroundColor(Color.WHITE);
                taghangye.setBackgroundColor(Color.WHITE);
                viewpager.setCurrentItem(1);
                break;

            case 2:
                tvpingtai.setTextColor(COLOR_UNSELE);
                tvchanping.setTextColor(COLOR_UNSELE);
                tvmeiti.setTextColor(COLOR_SELE);
                tvhangye.setTextColor(COLOR_UNSELE);
                tagpingtai.setBackgroundColor(Color.WHITE);
                tagchanping.setBackgroundColor(Color.WHITE);
                tagmeiti.setBackgroundColor(COLOR_TAG);
                taghangye.setBackgroundColor(Color.WHITE);
                viewpager.setCurrentItem(2);
                break;

            case 3:
                tvpingtai.setTextColor(COLOR_UNSELE);
                tvchanping.setTextColor(COLOR_UNSELE);
                tvmeiti.setTextColor(COLOR_UNSELE);
                tvhangye.setTextColor(COLOR_SELE);
                tagpingtai.setBackgroundColor(Color.WHITE);
                tagchanping.setBackgroundColor(Color.WHITE);
                tagmeiti.setBackgroundColor(Color.WHITE);
                taghangye.setBackgroundColor(COLOR_TAG);
                viewpager.setCurrentItem(3);
                break;
        }
    }
}
