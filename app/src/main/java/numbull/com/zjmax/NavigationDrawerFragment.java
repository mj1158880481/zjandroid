package numbull.com.zjmax;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

import numbull.com.zjmax.utils.Constants;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment extends Fragment {
    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mDrawerToggle;

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallbacks mCallbacks;

    private DrawerLayout mDrawerLayout;
    private View mDrawerListView;
    private View mFragmentContainerView;
    private TextView username, incomealready, incomewill;
    private ImageView headimg;

    private int mCurrentSelectedPosition = 0;

    private ZJApplication app;

    public NavigationDrawerFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Select either the default item (0) or the last selected item.

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mDrawerListView = (View) inflater.inflate(
                R.layout.fragment_navigation_drawer, container, false);
        headimg = (ImageView)mDrawerListView.findViewById(R.id.drawer_head_img);
        username = (TextView)mDrawerListView.findViewById(R.id.drawer_username);
        incomealready = (TextView)mDrawerListView.findViewById(R.id.drawer_income_already);
        incomewill = (TextView)mDrawerListView.findViewById(R.id.drawer_income_will);
        app = (ZJApplication)getActivity().getApplication();
        if(app.getUserinfo() != null){
            DecimalFormat df  = new DecimalFormat("###.00");
            username.setText(app.getUserinfo().getMobile() + "");
            if(!"0".equals(app.getUserinfo().getDearn()) && !"".equals(app.getUserinfo().getDearn()))
                incomealready.setText((df.format(Integer.parseInt(app.getUserinfo().getDearn()) * 0.01))  + "");
            else
                incomealready.setText("0");
            if(!"0".equals(app.getUserinfo().getTearn()) && !"".equals(app.getUserinfo().getTearn()))
                incomewill.setText((df.format(Integer.parseInt(app.getUserinfo().getTearn()) * 0.01))  + "");
            else
                incomewill.setText("0");
            mDrawerListView.findViewById(R.id.drawer_mine).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectItem(0);
                }
            });
            mDrawerListView.findViewById(R.id.drawer_income).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectItem(1);
                }
            });
            mDrawerListView.findViewById(R.id.drawer_branch).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectItem(2);
                }
            });
            mDrawerListView.findViewById(R.id.drawer_invite).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectItem(3);
                }
            });
            mDrawerListView.findViewById(R.id.drawer_set).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectItem(4);
                }
            });
            mDrawerListView.findViewById(R.id.drawer_about).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectItem(5);
                }
            });
        }else{
            username.setText("登录/注册");
            incomealready.setText("0");
            incomewill.setText("0");
            username.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ToLogin(10);

                }
            });
            headimg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ToLogin(10);
                }
            });
            mDrawerListView.findViewById(R.id.drawer_mine).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ToLogin(0);
                }
            });
            mDrawerListView.findViewById(R.id.drawer_income).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ToLogin(1);
                }
            });
            mDrawerListView.findViewById(R.id.drawer_branch).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ToLogin(2);
                }
            });
            mDrawerListView.findViewById(R.id.drawer_invite).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ToLogin(3);
                }
            });
            mDrawerListView.findViewById(R.id.drawer_set).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ToLogin(4);
                }
            });
            mDrawerListView.findViewById(R.id.drawer_about).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectItem(5);
                }
            });
        }


        return mDrawerListView;
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Constants.isdrawropen = true;
                if(Constants.isupdatedrawer){

                }else{
                    if(app.getUserinfo() != null){
                        DecimalFormat df  = new DecimalFormat("###.00");
                        username.setText(app.getUserinfo().getMobile() + "");
                        username.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        });
                        headimg.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        });
                        if(!"0".equals(app.getUserinfo().getDearn()) && !"".equals(app.getUserinfo().getDearn()))
                            incomealready.setText((df.format(Integer.parseInt(app.getUserinfo().getDearn()) * 0.01))  + "");
                        else
                            incomealready.setText("0");
                        if(!"0".equals(app.getUserinfo().getTearn()) && !"".equals(app.getUserinfo().getTearn()))
                            incomewill.setText((df.format(Integer.parseInt(app.getUserinfo().getTearn()) * 0.01))  + "");
                        else
                            incomewill.setText("0");
                        mDrawerListView.findViewById(R.id.drawer_mine).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectItem(0);
                            }
                        });
                        mDrawerListView.findViewById(R.id.drawer_income).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectItem(1);
                            }
                        });
                        mDrawerListView.findViewById(R.id.drawer_branch).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectItem(2);
                            }
                        });
                        mDrawerListView.findViewById(R.id.drawer_invite).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectItem(3);
                            }
                        });
                        mDrawerListView.findViewById(R.id.drawer_set).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectItem(4);
                            }
                        });
                        mDrawerListView.findViewById(R.id.drawer_about).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                selectItem(5);
                            }
                        });
                        Constants.isupdatedrawer = true;
                    }
                }
                if (!isAdded()) {
                    return;
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Constants.isdrawropen = false;
                if (!isAdded()) {
                    return;
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    private void selectItem(int position) {
        mCurrentSelectedPosition = position;
        if (mDrawerListView != null) {

        }
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
//            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // If the drawer is open, show the global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (mDrawerLayout != null && isDrawerOpen()) {
//            inflater.inflate(R.menu.global, menu);
//            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        if (item.getItemId() == R.id.action_example) {
//            Toast.makeText(getActivity(), "Example action.", Toast.LENGTH_SHORT).show();
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    private void ToLogin(int position){
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.putExtra("poitag", position);
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        getActivity().startActivity(intent);

    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(int position);
    }

}
