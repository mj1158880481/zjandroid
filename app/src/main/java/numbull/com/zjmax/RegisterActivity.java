package numbull.com.zjmax;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import numbull.com.zjmax.fragments.RegisterfirFragment;
import numbull.com.zjmax.fragments.RegistersecFragment;


public class RegisterActivity extends FragmentActivity implements RegisterfirFragment.Changesecfrag{
    private ZJApplication app;
    private final static int SCANNIN_GREQUEST_CODE = 1;
    private TextView action;
    private ImageView scanning;
    private RegisterfirFragment rfir;	//id = 0
    private RegistersecFragment rsec;	//id = 1
    private FragmentManager fmg;
    private boolean canscanning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_register);

        app = (ZJApplication)getApplication();

        fmg = getSupportFragmentManager();
        action = (TextView)findViewById(R.id.act_register_action);
        scanning = (ImageView)findViewById(R.id.act_register_scanning);

        if(android.os.Build.VERSION.SDK_INT >= 15){
            canscanning = true;
            scanning.setEnabled(true);
            scanning.setVisibility(View.VISIBLE);
            scanning.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
//                    Intent intent = new Intent();
//                    intent.setClass(RegisterActivity.this, ScanningActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivityForResult(intent, SCANNIN_GREQUEST_CODE);
                }

            });
        }else{
            canscanning = false;
            scanning.setEnabled(false);
            scanning.setVisibility(View.INVISIBLE);
        }


        action.setText("取消");
        action.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                RegisterActivity.this.startActivity(new Intent(RegisterActivity.this,
                        LoginActivity.class));
                RegisterActivity.this.finish();
            }

        });

        if(rfir == null){
            rfir = new RegisterfirFragment(app);
            rfir.setchangelistener(this);
        }
        changeFragment(rfir);
    }

    public void changeFragment(Fragment f){
        FragmentTransaction ft = fmg.beginTransaction();
        ft.replace(R.id.act_register_framelayout, f);
        ft.commit();
    }


    @Override
    public void changesecfragment() {
        if(rsec == null){
            rsec = new RegistersecFragment();
        }
        changeFragment(rsec);

        action.setText("上一步");
        scanning.setVisibility(View.INVISIBLE);
        action.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if(rfir == null){
                    rfir = new RegisterfirFragment(app);
                    rfir.setchangelistener(RegisterActivity.this);
                }
                changeFragment(rfir);
                if(canscanning){
                    scanning.setVisibility(View.VISIBLE);
                }
                action.setText("取消");
                action.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        RegisterActivity.this.startActivity(new Intent(RegisterActivity.this,
                                LoginActivity.class));
                        RegisterActivity.this.finish();
                    }

                });
            }

        });
        if(canscanning){
            scanning.setVisibility(View.INVISIBLE);
        }

    }
}
