package numbull.com.zjmax;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import numbull.com.zjmax.api.ZJClient;
import numbull.com.zjmax.callback.ResponseCallback;
import numbull.com.zjmax.httpcore.RequestParams;
import numbull.com.zjmax.modules.Loginmodule;


public class StartActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_start);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                StartActivity.this.startActivity(new Intent(StartActivity.this, WizardActivity.class));
                StartActivity.this.finish();
            }
        }, 1500);
    }

    private void Login(){
        RequestParams params = new RequestParams();
        params.put("a", "100");
        params.put("account", "18314885670");
        params.put("password", "yibite16888");
        ZJClient.Login(new ResponseCallback<Loginmodule>() {
            private static final String TAG = "ResponseCallback";

            public void onSuccess(Loginmodule response) {
                Log.i(TAG, "onSuccess with object returned");
                if ("0".equals(response.getE())) {
                    ZJApplication app = (ZJApplication)getApplication();
                    app.setUserinfo(response);
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            StartActivity.this.startActivity(new Intent(StartActivity.this, WizardActivity.class));
                            StartActivity.this.finish();
                        }
                    }, 1500); //2900 for release
//                    Intent resultIntent = new Intent();
//                    Bundle bundle = new Bundle();
//                    bundle.putString("uid", response.getId());
//					bundle.putParcelable("bitmap", barcode);
//                    resultIntent.putExtras(bundle);
//                    LoginActivity.this.setResult(RESULT_OK, resultIntent);
//					LoginActivity.this.startActivity(new Intent(LoginActivity.this, WizardActivity.class));
//                    LoginActivity.this.finish();
                } else if ("305".equals(response.getE())) {
//                    Toast.makeText(LoginActivity.this, "账号或密码错误", Toast.LENGTH_SHORT).show();
                } else if ("306".equals(response.getE())) {
//                    Toast.makeText(LoginActivity.this, "账号已登陆", Toast.LENGTH_SHORT).show();
                } else if ("305".equals(response.getE())) {
//                    Toast.makeText(LoginActivity.this, "错误次数过多，为了账户安全，已冻结该账户24小时", Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(LoginActivity.this, "网络不畅，请检查网络", Toast.LENGTH_SHORT).show();
                }
            }

            public void onSuccess(List<Loginmodule> response) {
                Log.i(TAG, "onSuccess with object list returned: " + response.size());

//                LoginActivity.this.finish();
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());

//                Toast.makeText(LoginActivity.this, "网络不畅，请检查网络" + statusCode, Toast.LENGTH_SHORT).show();
//                LoginActivity.this.finish();
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());

//                Toast.makeText(LoginActivity.this, "网络不畅，请检查网络" + statusCode, Toast.LENGTH_SHORT).show();
//                LoginActivity.this.finish();
            }

            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());

//                Toast.makeText(LoginActivity.this, "网络不畅，请检查网络" + statusCode, Toast.LENGTH_SHORT).show();
//                LoginActivity.this.finish();
            }
        }, params);
    }

}
