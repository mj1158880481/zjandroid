package numbull.com.zjmax;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import numbull.com.zjmax.api.ZJClient;
import numbull.com.zjmax.callback.ResponseCallback;
import numbull.com.zjmax.fragments.ProductDetailIntroFragment;
import numbull.com.zjmax.fragments.ProductDetailRecordFragment;
import numbull.com.zjmax.httpcore.RequestParams;
import numbull.com.zjmax.modules.ProductDetailmodule;
import numbull.com.zjmax.widgets.ArcProgress;


public class ProductDetailActivity extends FragmentActivity implements View.OnClickListener{
    private TextView title, rate, min, term, max, remain;
    private LinearLayout tagintro, tagrecord;
    private View tab1, tab2, d1, d2;
    private ArcProgress progress;
    private FragmentManager fmg;
    private ProductDetailIntroFragment introfragment;
    private ProductDetailRecordFragment recordfragment;
    String imgurl;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_product_detail);

        fmg = getSupportFragmentManager();
        id = getIntent().getStringExtra("product_id");
        title        = (TextView)findViewById(R.id.act_product_detail_title);
        rate 		 = (TextView)findViewById(R.id.act_product_detail_rate);
        min 		 = (TextView)findViewById(R.id.act_product_detail_min);
        term 		 = (TextView)findViewById(R.id.act_product_detail_term);
        max 		 = (TextView)findViewById(R.id.act_product_detail_max);
        remain 		 = (TextView)findViewById(R.id.act_product_detail_remain);
        tagintro     = (LinearLayout)findViewById(R.id.act_product_detail_tag_intro);
        tagrecord    = (LinearLayout)findViewById(R.id.act_product_detail_tag_record);
        tab1         = (View)findViewById(R.id.act_product_detail_tab1);
        tab2         = (View)findViewById(R.id.act_product_detail_tab2);
        d1           = (View)findViewById(R.id.act_product_detail_divider1);
        d2           = (View)findViewById(R.id.act_product_detail_divider2);
        progress     = (ArcProgress)findViewById(R.id.act_product_detail_progress);

        imgurl = getIntent().getStringExtra("product_img");
        title.setText(getIntent().getStringExtra("product_project"));
        rate.setText(getIntent().getStringExtra("product_rate") + "%");
        String minx = getIntent().getStringExtra("product_min");
        char[] mincs = minx.toCharArray();
        if(mincs.length < 7){
            min.setText(Integer.parseInt(minx) / 100 + "元");
        }else{
            min.setText(Integer.parseInt(minx) / 1000000 + "万元");
        }
        min.setText(Integer.parseInt(getIntent().getStringExtra("product_min"))/1000000 + "万元");
        term.setText(getIntent().getStringExtra("product_term") + "天");
        max.setText(getIntent().getStringExtra("product_max"));
        remain.setText(getIntent().getStringExtra("product_remain"));
        progress.setProgress(Integer.parseInt(getIntent().getStringExtra("product_progress")));

        findViewById(R.id.act_product_detail_back).setOnClickListener(this);

        if(!"".equals(id))
            loadData();
    }

    private void changetab(int position){
        switch(position){
            case 0:
                tagintro.setBackgroundColor(getResources().getColor(R.color.white));
                tagrecord.setBackgroundColor(getResources().getColor(R.color.product_detail_tab_unsele));
                tab1.setVisibility(View.VISIBLE);
                tab2.setVisibility(View.INVISIBLE);
                d1.setVisibility(View.INVISIBLE);
                d2.setVisibility(View.VISIBLE);
                break;

            case 1:
                tagintro.setBackgroundColor(getResources().getColor(R.color.product_detail_tab_unsele));
                tagrecord.setBackgroundColor(getResources().getColor(R.color.white));
                tab1.setVisibility(View.INVISIBLE);
                tab2.setVisibility(View.VISIBLE);
                d1.setVisibility(View.VISIBLE);
                d2.setVisibility(View.INVISIBLE);
                break;
        }
    }

    public void changeFragment(Fragment f){
        FragmentTransaction ft = fmg.beginTransaction();
        ft.replace(R.id.act_product_detail_frame, f);
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.act_product_detail_tag_intro:
                changetab(0);
                if(introfragment != null)
                    changeFragment(introfragment);
                break;

            case R.id.act_product_detail_tag_record:
                changetab(1);
                if(recordfragment != null)
                    changeFragment(recordfragment);
                break;

            case R.id.act_product_detail_back:
                ProductDetailActivity.this.finish();
                break;
        }
    }

    public void loadData(){

        RequestParams params = new RequestParams();
        params.put("id", id + "");
        params.put("a", "109");
        params.put("u", "0");
        ZJClient.GetProductDetail(new ResponseCallback<ProductDetailmodule>() {
            private static final String TAG = "ResponseCallback";

            public void onSuccess(ProductDetailmodule response) {
                Log.i(TAG, "onSuccess with object returned");
                if (response.getE().equals("0")) {
                    max.setText(Integer.parseInt(response.getAvailable()) / 1000000 + "万元");
                    introfragment = ProductDetailIntroFragment.newInstance(response.getMemo(), imgurl);
                    tagintro.setOnClickListener(ProductDetailActivity.this);
                    recordfragment = new ProductDetailRecordFragment(response.getRecords());
                    tagrecord.setOnClickListener(ProductDetailActivity.this);
                    changeFragment(introfragment);
                }
            }

            public void onSuccess(List<ProductDetailmodule> response) {
                Log.i(TAG, "onSuccess with object list returned: " + response.size());
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
            }

            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
            }
        }, params);
    }
}
