package numbull.com.zjmax.api;

import android.content.Context;

import numbull.com.zjmax.callback.ResponseCallback;
import numbull.com.zjmax.callback.ZJmaxHttpHandler;
import numbull.com.zjmax.httpcore.AsyncHttpClient;
import numbull.com.zjmax.httpcore.AsyncHttpResponseHandler;
import numbull.com.zjmax.httpcore.RequestParams;
import numbull.com.zjmax.modules.CheckNummodule;
import numbull.com.zjmax.modules.Emodule;
import numbull.com.zjmax.modules.GetIncomemodule;
import numbull.com.zjmax.modules.GetRecommodule;
import numbull.com.zjmax.modules.GetProductmodule;
import numbull.com.zjmax.modules.GetRecordmodule;
import numbull.com.zjmax.modules.GetTransfermodule;
import numbull.com.zjmax.modules.Getbranchmodule;
import numbull.com.zjmax.modules.Loginmodule;
import numbull.com.zjmax.modules.NewsDetailmodule;
import numbull.com.zjmax.modules.Newsmodule;
import numbull.com.zjmax.modules.ProductDetailmodule;
import numbull.com.zjmax.modules.Registerbackmodule;
import numbull.com.zjmax.modules.TransferDetailmodule;

/**
 * Created by leo on 2015/3/16.
 */

public class ZJClient {
    public static String URL = "http://192.168.0.220/e.html";

    private static AsyncHttpClient client;

    static {
        client = new AsyncHttpClient();
        client.setUserAgent(android.os.Build.DEVICE + ";" + android.os.Build.VERSION.RELEASE);
    }

    /**
     * 登录
     * */
    public static void Login(ResponseCallback<Loginmodule> callback,
                             RequestParams params){
        post(URL, params, new ZJmaxHttpHandler<Loginmodule>(callback){});
    }

    /**
     * 注册
     * */
    public static void Register(ResponseCallback<Registerbackmodule> callback,
                                RequestParams params){
        post(URL, params, new ZJmaxHttpHandler<Registerbackmodule>(callback){});
    }

    /**
     * 获取网点列表
     * */
    public static void GetBranch(ResponseCallback<Getbranchmodule> callback,
                                 RequestParams params){
        post(URL, params, new ZJmaxHttpHandler<Getbranchmodule>(callback){});
    }

    /**
     * 获取验证码
     * */
    public static void sendChecknum(ResponseCallback<CheckNummodule> callback,
                                    RequestParams params){
        post(URL, params, new ZJmaxHttpHandler<CheckNummodule>(callback){});
    }

    /**
     * 获取推荐页
     * */
    public static void GetRecom(ResponseCallback<GetRecommodule> callback,
                                      RequestParams params){
        post(URL, params, new ZJmaxHttpHandler<GetRecommodule>(callback) {
        });
    }

    /**
     * 获取产品列表
     * */
    public static void GetProductList(ResponseCallback<GetProductmodule> callback,
                                      RequestParams params){
        post(URL, params, new ZJmaxHttpHandler<GetProductmodule>(callback) {
        });
    }

    /**
     * 获取产品详情信息
     * */
    public static void GetProductDetail(ResponseCallback<ProductDetailmodule> callback,
                                        RequestParams params){
        post(URL, params, new ZJmaxHttpHandler<ProductDetailmodule>(callback){});
    }

    /**
     * 获取转让列表
     * */
    public static void GetTransferList(ResponseCallback<GetTransfermodule> callback,
                                      RequestParams params){
        post(URL, params, new ZJmaxHttpHandler<GetTransfermodule>(callback){});
    }

    /**
     * 获取转让详情信息
     * */
    public static void GetTransferDetail(ResponseCallback<TransferDetailmodule> callback,
                                       RequestParams params){
        post(URL, params, new ZJmaxHttpHandler<TransferDetailmodule>(callback){});
    }

    /**
     * 获取新闻资讯
     * */
    public static void GetNews(ResponseCallback<Newsmodule> callback,
                               RequestParams params){
        post(URL, params, new ZJmaxHttpHandler<Newsmodule>(callback){});
    }

    /**
     * 获取新闻详情
     * */
    public static void GetNewsDetail(ResponseCallback<NewsDetailmodule> callback,
                                     RequestParams params){
        post(URL, params, new ZJmaxHttpHandler<NewsDetailmodule>(callback){});
    }

    /**
     * 获取我的投资记录
     * */
    public static void GetRecords(ResponseCallback<GetRecordmodule> callback,
                                  RequestParams params){
        post(URL, params, new ZJmaxHttpHandler<GetRecordmodule>(callback){});
    }

    /**
     * 获取我的收益明细
     * */
    public static void GetIncomelist(ResponseCallback<GetIncomemodule> callback,
                                     RequestParams params){
        post(URL, params, new ZJmaxHttpHandler<GetIncomemodule>(callback){});
    }

    /**
     * 实名认证
     * */
    public static void DoRealname(ResponseCallback<Emodule> callback,
                                  RequestParams params){
        post(URL, params, new ZJmaxHttpHandler<Emodule>(callback));
    }

    /**
     * 绑定三方支付
     * */
    public static void Blindpay(ResponseCallback<Emodule> callback,
                                  RequestParams params){
        post(URL, params, new ZJmaxHttpHandler<Emodule>(callback));
    }

    /**
     * 绑定银行卡
     * */
    public static void Blindback(ResponseCallback<Emodule> callback,
                                  RequestParams params){
        post(URL, params, new ZJmaxHttpHandler<Emodule>(callback));
    }

    /***Help methods***/
    public static void get(String url, RequestParams params,AsyncHttpResponseHandler responseHandler) {
        client.get(url, params, responseHandler);
    }

    public static void post(String url, RequestParams params,AsyncHttpResponseHandler responseHandler) {
        client.post(url, params, responseHandler);
    }

    public static void put(String url, RequestParams params,AsyncHttpResponseHandler responseHandler) {
        client.put(url, params, responseHandler);
    }

    public static void delete(String url,AsyncHttpResponseHandler responseHandler) {
        client.delete(url, responseHandler);
    }
}
