package numbull.com.zjmax;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


public class RecordDetailActivity extends ActionBarActivity {
    private TextView project, data, sum, have_sum, rate, already, will, next, action;
    private String id, status, contract, enumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_record_detail);

        id = getIntent().getStringExtra("id");
        status = getIntent().getStringExtra("status");
        contract = getIntent().getStringExtra("contract");
        enumber = getIntent().getStringExtra("request_no");

        project = (TextView)findViewById(R.id.act_record_detail_name);
        data = (TextView)findViewById(R.id.act_record_detail_data);
        sum = (TextView)findViewById(R.id.act_record_detail_sum);
        have_sum = (TextView)findViewById(R.id.act_record_detail_have_sum);
        rate = (TextView)findViewById(R.id.act_record_detail_rate);
        already = (TextView)findViewById(R.id.act_record_detail_already);
        will = (TextView)findViewById(R.id.act_record_detail_will);
        next = (TextView)findViewById(R.id.act_record_detail_next);
        action = (TextView)findViewById(R.id.act_record_detail_action);

        project.setText(getIntent().getStringExtra("project"));
        data.setText(getIntent().getStringExtra("data"));
        sum.setText(getIntent().getStringExtra("sum"));
        have_sum.setText(getIntent().getStringExtra("have_sum"));
        rate.setText(getIntent().getStringExtra("rate") + "%");
        already.setText(getIntent().getStringExtra("already"));
        will.setText(getIntent().getStringExtra("will"));
        next.setText(getIntent().getStringExtra("next"));
        action.setText(getIntent().getStringExtra("project"));

        if(!"".equals(status)){
            if("0".equals(status)){
                action.setText("转让项目");
            }else if("1".equals(status)){
                action.setText("去完成支付");
            }else if("2".equals(status)){
                action.setText("撤销转让");
            }else if("3".equals(status)){
                action.setText("已转让");
            }else if("4".equals(status)){
                action.setText("已结算");
            }
        }

        findViewById(R.id.act_record_detail_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RecordDetailActivity.this.finish();
            }
        });

    }

}
