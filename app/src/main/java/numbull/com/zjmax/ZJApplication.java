package numbull.com.zjmax;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import numbull.com.zjmax.modules.Getbranchmodule;
import numbull.com.zjmax.modules.Loginmodule;

/**
 * Created by leo on 2015/3/17.
 */
public class ZJApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        initImageLoader(getApplicationContext());
    }

    private Loginmodule userinfo;
    private Getbranchmodule branchs;

    public Loginmodule getUserinfo() {
        return userinfo;
    }

    public void setUserinfo(Loginmodule userinfo) {
        this.userinfo = userinfo;
    }

    public Getbranchmodule getBranchs() {
        return branchs;
    }

    public void setBranchs(Getbranchmodule branchs) {
        this.branchs = branchs;
    }

    public static void initImageLoader(Context context){
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true).cacheOnDisc(true)
//		.imageScaleType(ImageScaleType.EXACTLY)
                .imageScaleType(ImageScaleType.NONE)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .showStubImage(R.drawable.bkg_zjmax).build();

//		int size = context.getResources().getDisplayMetrics().widthPixels;
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                context).threadPriority(Thread.NORM_PRIORITY - 2)
                .defaultDisplayImageOptions(defaultOptions).threadPoolSize(3)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .memoryCacheExtraOptions(100, 100)
                        // .enableLogging() // Not necessary in common
                .build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
    }

}
