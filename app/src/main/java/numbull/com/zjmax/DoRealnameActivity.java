package numbull.com.zjmax;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import numbull.com.zjmax.api.ZJClient;
import numbull.com.zjmax.callback.ResponseCallback;
import numbull.com.zjmax.httpcore.RequestParams;
import numbull.com.zjmax.modules.Emodule;


public class DoRealnameActivity extends Activity {
    private EditText name, number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_do_realname);

        name = (EditText)findViewById(R.id.act_do_realname_name);
        number = (EditText)findViewById(R.id.act_do_realname_number);

        findViewById(R.id.act_do_realname_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if("".equals(name.getText().toString())){
                    name.setHint("触摸这里输入姓名");
                }else if("".equals(number.getText().toString())){
                    number.setHint("触摸这里输入身份证号");
                }else{
                    RequestParams params = new RequestParams();
                    params.put("a", "105");
                    params.put("type", "1");
                    params.put("name", name.getText().toString());
                    params.put("number", number.getText().toString());
                    ZJClient.DoRealname(new ResponseCallback<Emodule>(){
                        private static final String TAG = "ResponseCallback";

                        public void onSuccess(Emodule response) {
                            Log.i(TAG, "onSuccess with object returned");
                            if("0".equals(response.getE())){
                                Toast.makeText(DoRealnameActivity.this, "提交成功，等待审核", Toast.LENGTH_SHORT).show();
                                DoRealnameActivity.this.finish();
                            }else{
                                Toast.makeText(DoRealnameActivity.this, "error" + response.getE(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        public void onSuccess(List<Emodule> response) {
                            Log.i(TAG, "onSuccess with object list returned: " + response.size());
                        }

                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                                    + throwable.getMessage());
                            Toast.makeText(DoRealnameActivity.this, errorResponse.toString() + "", Toast.LENGTH_SHORT).show();
                        }

                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                            Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                                    + throwable.getMessage());
                            Toast.makeText(DoRealnameActivity.this, errorResponse.toString() + "", Toast.LENGTH_SHORT).show();
                        }

                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                                    + throwable.getMessage());
                            Toast.makeText(DoRealnameActivity.this, responseString + "", Toast.LENGTH_SHORT).show();
                        }
                    }, params);
                }
            }
        });
    }

}
