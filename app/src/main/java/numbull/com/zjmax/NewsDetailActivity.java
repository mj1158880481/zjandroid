package numbull.com.zjmax;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import numbull.com.zjmax.api.ZJClient;
import numbull.com.zjmax.callback.ResponseCallback;
import numbull.com.zjmax.httpcore.RequestParams;
import numbull.com.zjmax.modules.NewsDetailmodule;
import numbull.com.zjmax.utils.ZjTagHandler;


public class NewsDetailActivity extends ActionBarActivity {
    private String id = null;
    private TextView title, date, text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_news_detail);

        id = getIntent().getStringExtra("news_id");

        title = (TextView)findViewById(R.id.act_newsdetail_title);
        date  = (TextView)findViewById(R.id.act_newsdetail_data);
        text  = (TextView)findViewById(R.id.act_newsdetail_text);

        findViewById(R.id.act_news_detail_back).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                NewsDetailActivity.this.finish();
            }

        });

        if(!"".equals(id)){
            RequestParams params = new RequestParams();
            params.put("a", "123");
            params.put("u", "0");
            params.put("id", id);
            ZJClient.GetNewsDetail(new ResponseCallback<NewsDetailmodule>() {
                private static final String TAG = "ResponseCallback";

                public void onSuccess(NewsDetailmodule response) {
                    Log.i(TAG, "onSuccess with object returned");
                    if ("0".equals(response.getE())) {
                        title.setText(response.getTitle());
                        date.setText(response.getDate());
                        text.setText(Html.fromHtml(response.getContent(), null, new ZjTagHandler()));
                        text.setAutoLinkMask(Linkify.ALL);
                        text.setMovementMethod(LinkMovementMethod.getInstance());
                    }
                }

                public void onSuccess(List<NewsDetailmodule> response) {
                    Log.i(TAG, "onSuccess with object list returned: " + response.size());
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                            + throwable.getMessage());
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                            + throwable.getMessage());
                }

                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                            + throwable.getMessage());
                }
            }, params);
        }
    }

}
