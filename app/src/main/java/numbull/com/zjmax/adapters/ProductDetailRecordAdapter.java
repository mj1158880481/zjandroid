package numbull.com.zjmax.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import numbull.com.zjmax.R;
import numbull.com.zjmax.modules.ProductRecord;

/**
 * Created by leo on 2015/3/18.
 */
public class ProductDetailRecordAdapter extends ArrayAdapter<ProductRecord> {

    private Context context;
    private List<ProductRecord> list;

    public ProductDetailRecordAdapter(Context context, int resource,
                                      List<ProductRecord> list) {
        super(context, resource, list);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return super.getItemId(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        RViewHolder holder = null;
        if(convertView == null){
            holder = new RViewHolder();
            convertView = View.inflate(context, R.layout.item_productdetail_record, null);
            holder.user = (TextView)convertView.findViewById(R.id.item_productdetail_record_user);
            holder.sum  = (TextView)convertView.findViewById(R.id.item_productdetail_record_sum);
            holder.data = (TextView)convertView.findViewById(R.id.item_productdetail_record_data);

            convertView.setTag(holder);
        }else{
            holder = (RViewHolder)convertView.getTag();
        }

        holder.user.setText(list.get(position).getName());
        holder.sum.setText(list.get(position).getAmount());
        holder.data.setText(list.get(position).getOccur());

        return View.inflate(context, R.layout.item_productdetail_record, null);
    }

    private class RViewHolder{
        private TextView user;
        private TextView sum;
        private TextView data;
    }
}
