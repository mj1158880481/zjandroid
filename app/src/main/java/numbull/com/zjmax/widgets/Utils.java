package numbull.com.zjmax.widgets;

import android.content.res.Resources;
import android.util.TypedValue;
import android.view.View;

/**
 * Created by leo on 14-11-6.
 */
public class Utils {
    /**
     * Convert Dp to Pixel for arc
     * */
    public static float dp2px(Resources resources, float dp) {
        final float scale = resources.getDisplayMetrics().density;
        return  dp * scale + 0.5f;
    }

    public static float sp2px(Resources resources, float sp){
        final float scale = resources.getDisplayMetrics().scaledDensity;
        return sp * scale;
    }

}
