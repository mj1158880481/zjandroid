package numbull.com.zjmax.widgets;

import java.util.ArrayList;
import java.util.List;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import numbull.com.zjmax.R;
import numbull.com.zjmax.ZJApplication;
import numbull.com.zjmax.modules.BranchNodesmodule;
import numbull.com.zjmax.utils.Constants;

public class Dialogbranch extends Dialog{
	private ZJApplication app;
	private Context context;
	private Spinner prvs, city;
	private ListView listview;
	private List<String> prvlist, citylist;
	private int prvloc = 0;
	private int cityloc = 0;
	private UpdateInfo update;

    public Dialogbranch(Context context, int theme, UpdateInfo update, ZJApplication app) {
        super(context, theme);
        this.context = context;
        this.update  = update;
        this.app     = app;
    }

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.dialog_branch);
		
		prvs = (Spinner)findViewById(R.id.dialog_branch_spprv);
		city = (Spinner)findViewById(R.id.dialog_branch_spcity);
		listview = (ListView)findViewById(R.id.dialog_branch_listview);
		
		prvlist = new ArrayList<String>();
		citylist = new ArrayList<String>();
		
		Constants.prv_id = app.getBranchs().getList().get(0).getId();
		Constants.city_id = app.getBranchs().getList().get(0).getCity().get(0).getId();
		Constants.branch_id = app.getBranchs().getList().get(0).getCity().get(0).getNodes().get(0).getId();
		
		prvlist.clear();
		for(int i=0;i<app.getBranchs().getList().size();i++){
			prvlist.add(app.getBranchs().getList().get(i).getName());
		}
		
		prvs.setAdapter(new ArrayAdapter<String>(context,R.layout.item_spinner, prvlist));
		
		prvs.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				citylist.clear();
				prvloc = position;
				Constants.prv = app.getBranchs().getList().get(position).getName();
				Constants.prv_id = app.getBranchs().getList().get(position).getId();
				for(int j=0;j<app.getBranchs().getList().get(position).getCity().size();j++){
					citylist.add(app.getBranchs().getList().get(position).getCity().get(j).getName());
				}
				city.setAdapter(new ArrayAdapter<String>(context,R.layout.item_spinner, citylist));
				city.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						cityloc = position;
						Constants.city = app.getBranchs().getList().get(prvloc).getCity().get(position).getName();
						Constants.city_id = app.getBranchs().getList().get(prvloc).getCity().get(position).getId();
						listview.setAdapter(new ListAdapter(context, 0, 
								app.getBranchs().getList().get(prvloc).getCity().get(position).getNodes()));
						listview.setOnItemClickListener(new OnItemClickListener(){

							@Override
							public void onItemClick(AdapterView<?> parent,
									View view, int position, long id) {
								// TODO Auto-generated method stub
								Constants.branch = app.getBranchs().getList().get(prvloc).getCity().get(cityloc).getNodes().get(position).getName();
								Constants.branch_id = app.getBranchs().getList().get(prvloc).getCity().get(cityloc).getNodes().get(position).getId();
								update.UpdateUI();
								Dialogbranch.this.dismiss();
							}
							
						});
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub
						
					}
					
				});
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
			
		});
	}
	
	public interface UpdateInfo {
		void UpdateUI();
	}
	
	class ListAdapter extends ArrayAdapter<BranchNodesmodule> {
		private List<BranchNodesmodule> list;
		private Context context;

		public ListAdapter(Context context, int resource,
				List<BranchNodesmodule> list) {
			super(context, resource, list);
			// TODO Auto-generated constructor stub
			this.context = context;
			this.list = list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder holder = null;
			if(convertView == null){
				holder = new ViewHolder();
				convertView = View.inflate(context, R.layout.item_branch_list, null);
				
				holder.name = (TextView)convertView.findViewById(R.id.item_branch_list_name);
				holder.address = (TextView)convertView.findViewById(R.id.item_branch_list_address);
				holder.phone = (TextView)convertView.findViewById(R.id.item_branch_list_phone);
				
				convertView.setTag(holder);
			}else{
				holder = (ViewHolder)convertView.getTag();
			}
			
			holder.name.setText(list.get(position).getName());
			holder.address.setText(list.get(position).getAddress());
			holder.phone.setText(list.get(position).getPhone());
			
			return convertView;
		}
	}
	
	class ViewHolder {
		private TextView name;
		private TextView address;
		private TextView phone;
	}

}
