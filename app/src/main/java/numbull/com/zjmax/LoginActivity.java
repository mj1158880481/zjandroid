package numbull.com.zjmax;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import numbull.com.zjmax.api.ZJClient;
import numbull.com.zjmax.callback.ResponseCallback;
import numbull.com.zjmax.httpcore.RequestParams;
import numbull.com.zjmax.modules.Loginmodule;
import numbull.com.zjmax.utils.Constants;


public class LoginActivity extends ActionBarActivity {
    private EditText username, password;
    private int poi = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_login);

        if(getIntent() != null){
            poi = getIntent().getIntExtra("poitag", 10);
        }

        username = (EditText)findViewById(R.id.act_login_username);
        password = (EditText)findViewById(R.id.act_login_password);

        username.setText("13814885671");
        password.setText("yibite16888");

        findViewById(R.id.act_login_login).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if("".equals(username.getText().toString())){
                    Toast.makeText(LoginActivity.this, "请填写账号", Toast.LENGTH_SHORT).show();
                }else if("".equals(password.getText().toString())){
                    Toast.makeText(LoginActivity.this, "请填写密码", Toast.LENGTH_SHORT).show();
                }else{
                    Login();
                }
            }

        });

        findViewById(R.id.act_login_register).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                LoginActivity.this.startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                LoginActivity.this.finish();
            }

        });

        findViewById(R.id.act_login_exit).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                LoginActivity.this.finish();
            }

        });
    }
    private void Login(){
        RequestParams params = new RequestParams();
        params.put("a", "100");
        params.put("account", username.getText().toString());
        params.put("password", password.getText().toString());
        ZJClient.Login(new ResponseCallback<Loginmodule>() {
            private static final String TAG = "ResponseCallback";

            public void onSuccess(Loginmodule response) {
                Log.i(TAG, "onSuccess with object returned");
                if ("0".equals(response.getE())) {
                    Constants.u = response.getU();
                    ZJApplication zjapp = (ZJApplication) getApplication();
                    zjapp.setUserinfo(response);
                    if(poi == 0){
                        LoginActivity.this.startActivity(new Intent(LoginActivity.this, RecordActivity.class));
                    }else if(poi == 1){
                        LoginActivity.this.startActivity(new Intent(LoginActivity.this, IncomeActivity.class));
                    }else if(poi == 2){
                        LoginActivity.this.startActivity(new Intent(LoginActivity.this, BranchActivity.class));
                    }else if(poi == 3){

                    }else if(poi == 4){
                        LoginActivity.this.startActivity(new Intent(LoginActivity.this, SetActivity.class));
                    }
                    LoginActivity.this.finish();
                } else if ("305".equals(response.getE())) {
                    Toast.makeText(LoginActivity.this, "账号或密码错误", Toast.LENGTH_SHORT).show();
                } else if ("306".equals(response.getE())) {
                    Toast.makeText(LoginActivity.this, "账号已登陆", Toast.LENGTH_SHORT).show();
                } else if ("305".equals(response.getE())) {
                    Toast.makeText(LoginActivity.this, "错误次数过多，为了账户安全，已冻结该账户24小时", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LoginActivity.this, "网络不畅，请检查网络", Toast.LENGTH_SHORT).show();
                }
            }

            public void onSuccess(List<Loginmodule> response) {
                Log.i(TAG, "onSuccess with object list returned: " + response.size());
                LoginActivity.this.finish();
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                Toast.makeText(LoginActivity.this, "网络不畅，请检查网络" + statusCode, Toast.LENGTH_SHORT).show();
                LoginActivity.this.finish();
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                Toast.makeText(LoginActivity.this, "网络不畅，请检查网络" + statusCode, Toast.LENGTH_SHORT).show();
                LoginActivity.this.finish();
            }

            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                Toast.makeText(LoginActivity.this, "网络不畅，请检查网络" + statusCode, Toast.LENGTH_SHORT).show();
                LoginActivity.this.finish();
            }
        }, params);
    }
}
