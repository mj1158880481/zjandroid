package numbull.com.zjmax.modules;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Productmodule {
	@Expose
	@SerializedName("id")
	private String id;				//产品id
	
	@Expose
	@SerializedName("series")
	private String series;			//产品系列

    @Expose
    @SerializedName("series_color")
    private String series_color;			//产品系列
	
	@Expose
	@SerializedName("package")
	private String packages;		//产品包名
	
	@Expose
	@SerializedName("project")
	private String project;			//投资项目
	
	@Expose
	@SerializedName("progress")
	private String progress;		//进度  百分之
	
	@Expose
	@SerializedName("rate")
	private String rate;			//预期年化收益 千分之
	
	@Expose
	@SerializedName("risk")
	private String risk;			//参考评级
	
	@Expose
	@SerializedName("term")
	private String term;			//投资期限 天数
	
	@Expose
	@SerializedName("min")
	private String min;				//起投金额 单位分
	
	@Expose
	@SerializedName("remain")
	private String remain;			//剩余金额 单位分
	
	@Expose
	@SerializedName("img")
	private String img;				//项目效果图
	
	@Expose
	@SerializedName("available")
	private String available;		//总额可投

    @Expose
    @SerializedName("status")
    private String status;          //状态  0=未开始；1=可投；2=投满；3=截止
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSeries() {
		return series;
	}
	public void setSeries(String series) {
		this.series = series;
	}
    public String getSeries_color() { return series_color; }
    public void setSeries_color(String series_color) { this.series_color = series_color; }
    public String getPackages() {
		return packages;
	}
	public void setPackages(String packages) {
		this.packages = packages;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getProgress() {
		return progress;
	}
	public void setProgress(String progress) {
		this.progress = progress;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getRisk() {
		return risk;
	}
	public void setRisk(String risk) {
		this.risk = risk;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getMin() {
		return min;
	}
	public void setMin(String min) {
		this.min = min;
	}
	public String getRemain() {
		return remain;
	}
	public void setRemain(String remain) {
		this.remain = remain;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getAvailable() {
		return available;
	}
	public void setAvailable(String available) {
		this.available = available;
	}
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}
