package numbull.com.zjmax.modules;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BranchPrvmodule {
	
	@Expose
	@SerializedName("id")
	private String id = "";
	
	@Expose
	@SerializedName("name")
	private String name = "";
	
	@Expose
	@SerializedName("city")
	private List<BranchCitymodule> city;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<BranchCitymodule> getCity() {
		return city;
	}

	public void setCity(List<BranchCitymodule> city) {
		this.city = city;
	}
	
	
}
