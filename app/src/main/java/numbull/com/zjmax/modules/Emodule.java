package numbull.com.zjmax.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by leo on 2015/3/25.
 */
public class Emodule {
    @Expose
    @SerializedName("e")
    private String e;

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }
}
