package numbull.com.zjmax.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckNummodule {
	
	@Expose
	@SerializedName("e")
	private String e = "";

	public String getE() {
		return e;
	}

	public void setE(String e) {
		this.e = e;
	}
	
	
}
