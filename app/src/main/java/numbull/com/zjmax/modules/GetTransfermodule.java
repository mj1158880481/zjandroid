package numbull.com.zjmax.modules;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetTransfermodule {
	
	@Expose
	@SerializedName("e")
	private String e = "";
	
	@Expose
	@SerializedName("total")
	private String total = "";
	
	@Expose
	@SerializedName("list")
	private List<Transfermodule> list;

	public String getE() {
		return e;
	}

	public void setE(String e) {
		this.e = e;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public List<Transfermodule> getList() {
		return list;
	}

	public void setList(List<Transfermodule> list) {
		this.list = list;
	}
	
	
}
