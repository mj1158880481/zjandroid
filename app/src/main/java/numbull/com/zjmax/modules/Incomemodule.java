package numbull.com.zjmax.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Incomemodule {
	
	@Expose
	@SerializedName("name")
	private String name = "";
	
	@Expose
	@SerializedName("dateline")
	private String dateline = "";
	
	@Expose
	@SerializedName("dearn")
	private String dearn = "";
	
	@Expose
	@SerializedName("nearn")
	private String nearn = "";
	
	@Expose
	@SerializedName("tearn")
	private String tearn = "";
	
	@Expose
	@SerializedName("amount")
	private String amount = "";
	
	@Expose
	@SerializedName("term")
	private String term = "";
	
	@Expose
	@SerializedName("remain")
	private String remain = "";
	
	@Expose
	@SerializedName("rate")
	private String rate = "";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDateline() {
		return dateline;
	}

	public void setDateline(String dateline) {
		this.dateline = dateline;
	}

	public String getDearn() {
		return dearn;
	}

	public void setDearn(String dearn) {
		this.dearn = dearn;
	}

	public String getNearn() {
		return nearn;
	}

	public void setNearn(String nearn) {
		this.nearn = nearn;
	}

	public String getTearn() {
		return tearn;
	}

	public void setTearn(String tearn) {
		this.tearn = tearn;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getRemain() {
		return remain;
	}

	public void setRemain(String remain) {
		this.remain = remain;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}
	
	
}
