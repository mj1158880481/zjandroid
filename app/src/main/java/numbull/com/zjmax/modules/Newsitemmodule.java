package numbull.com.zjmax.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Newsitemmodule {
	
	@Expose
	@SerializedName("id")
	private String id;
	
	@Expose
	@SerializedName("title")
	private String title;
	
	@Expose
	@SerializedName("date")
	private String date;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
}
