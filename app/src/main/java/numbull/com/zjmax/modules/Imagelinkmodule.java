package numbull.com.zjmax.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by leo on 2015/3/17.
 */
public class Imagelinkmodule {

    @Expose
    @SerializedName("image")
    private String image;

    @Expose
    @SerializedName("title")
    private String title;

    @Expose
    @SerializedName("link")
    private String link;

    public String getImage() { return image; }
    public void setImage(String image) { this.image = image; }
    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }
    public String getLink() { return link; }
    public void setLink(String link) { this.link = link; }
}
