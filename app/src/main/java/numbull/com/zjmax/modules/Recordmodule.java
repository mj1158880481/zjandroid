package numbull.com.zjmax.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Recordmodule {
	
	@Expose
	@SerializedName("id")
	private String id = "";				//id
	
	@Expose
	@SerializedName("name")
	private String name = "";			//项目名称
	
	@Expose
	@SerializedName("status")
	private String status = "";			//持有状态
	
	@Expose
	@SerializedName("dateline")
	private String dateline = "";		//购买日期
	
	@Expose
	@SerializedName("invoice")
	private String invoice = "";		//电子编号
	
	@Expose
	@SerializedName("request_no")
	private String request_no = "";		//保全代码
	
	@Expose
	@SerializedName("amount")
	private String amount = "";			//投资金额
	
	@Expose
	@SerializedName("term")
	private String term = "";			//投资期限 单位天
	
	@Expose
	@SerializedName("rate")
	private String rate = "";			//预期利率 千分之
	
	@Expose
	@SerializedName("earn")
	private String earn = "";			//预期收益 单位分

    @Expose
    @SerializedName("contract")
    private String contract;            //合同

    @Expose
    @SerializedName("hold")
    private String hold;                //持有金额

    @Expose
    @SerializedName("dearn")
    private String dearn;               //已收收益

    @Expose
    @SerializedName("tearn")
    private String tearn;               //待收收益

    @Expose
    @SerializedName("nearn")
    private String nearn;               //下次收益日

    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDateline() {
		return dateline;
	}
	public void setDateline(String dateline) {
		this.dateline = dateline;
	}
	public String getInvoice() {
		return invoice;
	}
	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}
	public String getRequest_no() {
		return request_no;
	}
	public void setRequest_no(String request_no) {
		this.request_no = request_no;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getEarn() {
		return earn;
	}
	public void setEarn(String earn) {
		this.earn = earn;
	}
    public String getContract() {
        return contract;
    }
    public void setContract(String contract) {
        this.contract = contract;
    }
    public String getHold() {
        return hold;
    }
    public void setHold(String hold) {
        this.hold = hold;
    }
    public String getDearn() {
        return dearn;
    }
    public void setDearn(String dearn) {
        this.dearn = dearn;
    }
    public String getTearn() {
        return tearn;
    }
    public void setTearn(String tearn) {
        this.tearn = tearn;
    }
    public String getNearn() {
        return nearn;
    }
    public void setNearn(String nearn) {
        this.nearn = nearn;
    }
}
