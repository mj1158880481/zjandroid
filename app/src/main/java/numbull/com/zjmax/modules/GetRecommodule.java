package numbull.com.zjmax.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by leo on 2015/3/17.
 */
public class GetRecommodule {

    @Expose
    @SerializedName("e")
    private String e;

    @Expose
    @SerializedName("image")
    private List<Imagelinkmodule> image;

    @Expose
    @SerializedName("list")
    private List<Productmodule> list;

    public String getE() { return e; }
    public void setE(String e) { this.e = e; }
    public List<Imagelinkmodule> getImage() { return image; }
    public void setImage(List<Imagelinkmodule> image) { this.image = image; }
    public List<Productmodule> getList() { return list; }
    public void setList(List<Productmodule> list) { this.list = list; }
}
