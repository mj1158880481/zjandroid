package numbull.com.zjmax.modules;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetailmodule {
	
	@Expose
	@SerializedName("e")
	private String e;
	
	@Expose
	@SerializedName("progress")
	private String progress;
	
	@Expose
	@SerializedName("rate")
	private String rate;
	
	@Expose
	@SerializedName("term")
	private String term;
	
	@Expose
	@SerializedName("remain")
	private String remain;
	
	@Expose
	@SerializedName("sday")
	private String sday;
	
	@Expose
	@SerializedName("eday")
	private String eday;
	
	@Expose
	@SerializedName("income")
	private String income;
	
	@Expose
	@SerializedName("memo")
	private String memo;
	
	@Expose
	@SerializedName("subject")
	private String subject;
	
	@Expose
	@SerializedName("available")
	private String available;
	
	@Expose
	@SerializedName("records")
	private List<ProductRecord> records;
	
	
	public String getE() {
		return e;
	}
	public void setE(String e) {
		this.e = e;
	}
	public String getProgress() {
		return progress;
	}
	public void setProgress(String progress) {
		this.progress = progress;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getRemain() {
		return remain;
	}
	public void setRemain(String remain) {
		this.remain = remain;
	}
	public String getSday() {
		return sday;
	}
	public void setSday(String sday) {
		this.sday = sday;
	}
	public String getEday() {
		return eday;
	}
	public void setEday(String eday) {
		this.eday = eday;
	}
	public String getIncome() {
		return income;
	}
	public void setIncome(String income) {
		this.income = income;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getAvailable() {
		return available;
	}
	public void setAvailable(String available) {
		this.available = available;
	}
	public List<ProductRecord> getRecords() {
		return records;
	}
	public void setRecords(List<ProductRecord> records) {
		this.records = records;
	}
	
	
}
