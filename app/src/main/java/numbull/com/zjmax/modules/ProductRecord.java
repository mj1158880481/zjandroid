package numbull.com.zjmax.modules;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductRecord{
	
	@Expose
	@SerializedName("name")
	private String name;
	
	@Expose
	@SerializedName("amount")
	private String amount;
	
	@Expose
	@SerializedName("occur")
	private String occur;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getOccur() {
		return occur;
	}
	public void setOccur(String occur) {
		this.occur = occur;
	}

}
