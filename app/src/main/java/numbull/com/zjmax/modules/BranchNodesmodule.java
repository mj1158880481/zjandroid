package numbull.com.zjmax.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BranchNodesmodule {
	
	@Expose
	@SerializedName("id")
	private String id = "";
	
	@Expose
	@SerializedName("name")
	private String name = "";
	
	@Expose
	@SerializedName("address")
	private String address = "";
	
	@Expose
	@SerializedName("phone")
	private String phone = "";

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
	
}
