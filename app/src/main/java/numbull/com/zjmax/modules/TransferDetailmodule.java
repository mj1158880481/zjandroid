package numbull.com.zjmax.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransferDetailmodule {
	
	@Expose
	@SerializedName("e")
	private String e = "";
	
	@Expose
	@SerializedName("rate")
	private String rate = "";				//预期年化利率，百分之
	
	@Expose
	@SerializedName("risk")
	private String risk = "";				//评级
	
	@Expose
	@SerializedName("remain")
	private String remain = "";				//剩余天数
	
	@Expose
	@SerializedName("sday")
	private String sday = "";				//转让生效日
	
	@Expose
	@SerializedName("project")
	private String project = "";			//项目名称
	
	@Expose
	@SerializedName("memo")
	private String memo = "";				//项目介绍
	
	@Expose
	@SerializedName("image")
	private String image = "";				//图片
	
	@Expose
	@SerializedName("price")
	private String price = "";				//转让价格 单位分
	
	@Expose
	@SerializedName("value")
	private String value = "";				//项目价值 单位分
	
	@Expose
	@SerializedName("discount")
	private String discount = "";			//降价额度 单位分

	public String getE() {
		return e;
	}

	public void setE(String e) {
		this.e = e;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getRisk() {
		return risk;
	}

	public void setRisk(String risk) {
		this.risk = risk;
	}

	public String getRemain() {
		return remain;
	}

	public void setRemain(String remain) {
		this.remain = remain;
	}

	public String getSday() {
		return sday;
	}

	public void setSday(String sday) {
		this.sday = sday;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}
	
}
