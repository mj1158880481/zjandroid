package numbull.com.zjmax.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transfermodule {
	
	@Expose
	@SerializedName("id")
	private String id = "";				//转让产品唯一条目就
	
	@Expose
	@SerializedName("project")
	private String project = "";		//转让项目 eg:余姚建设01
	
	@Expose
	@SerializedName("rate")
	private String rate = "";			//预期年化利率 百分之
	
	@Expose
	@SerializedName("remain")
	private String remain = "";			//剩余天数
	
	@Expose
	@SerializedName("price")
	private String price = "";			//转让价格 单位分

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getRemain() {
		return remain;
	}

	public void setRemain(String remain) {
		this.remain = remain;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
}
