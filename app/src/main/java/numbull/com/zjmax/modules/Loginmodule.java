package numbull.com.zjmax.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Loginmodule {
	
	@Expose
	@SerializedName("e")
	private String e = "";						//错误码
	
	@Expose
	@SerializedName("u")
	private String u = "";						//账号标识
	
	@Expose
	@SerializedName("id")
	private String id = "";						//用户id
	
	@Expose
	@SerializedName("username")
	private String username = "";				//登陆用户名
	
	@Expose
	@SerializedName("truename")
	private String truename = "";				//真名
	
	@Expose
	@SerializedName("mobile")
	private String mobile   = "";				//电话号码
	
	@Expose
	@SerializedName("real_verified")
	private String real_verified = "";			//是否通过实名认证
	
	@Expose
	@SerializedName("outlet_id")
	private String outlet_id = "";				//所属网点id 为0代表没有
	
	@Expose
	@SerializedName("outlet_promoter_id")
	private String outlet_promoter_id = "";		//归属业务员id，为0代表没有归属业务员
	
	@Expose
	@SerializedName("outletname")
	private String outletname = "";				//网点名称
	
	@Expose
	@SerializedName("total")
	private String total = "";					//总资产 单位分
	
	@Expose
	@SerializedName("nearn")
	private String nearn = "";					//下次收益 单位分
	
	@Expose
	@SerializedName("tearn")
	private String tearn = "";					//待收益总额 单位分
	
	@Expose
	@SerializedName("dearn")
	private String dearn = "";					//已收收益 单位分
	
	@Expose
	@SerializedName("available")
	private String available = "";				//可用金额 单位分
	
	@Expose
	@SerializedName("balance")
	private String balance = "";				//账户余额 单位分
	
	@Expose
	@SerializedName("freez")
	private String freez = "";					//冻结资金 单位分
	
	@Expose
	@SerializedName("icounter")
	private String icounter = "";				//投资项目数
	
	
	public String getE() {
		return e;
	}
	public void setE(String e) {
		this.e = e;
	}
	public String getU() {
		return u;
	}
	public void setU(String u) {
		this.u = u;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTruename() {
		return truename;
	}
	public void setTruename(String truename) {
		this.truename = truename;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getReal_verified() {
		return real_verified;
	}
	public void setReal_verified(String real_verified) {
		this.real_verified = real_verified;
	}
	public String getOutlet_id() {
		return outlet_id;
	}
	public void setOutlet_id(String outlet_id) {
		this.outlet_id = outlet_id;
	}
	public String getOutlet_promoter_id() {
		return outlet_promoter_id;
	}
	public void setOutlet_promoter_id(String outlet_promoter_id) {
		this.outlet_promoter_id = outlet_promoter_id;
	}
	public String getOutletname() {
		return outletname;
	}
	public void setOutletname(String outletname) {
		this.outletname = outletname;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getNearn() {
		return nearn;
	}
	public void setNearn(String nearn) {
		this.nearn = nearn;
	}
	public String getTearn() {
		return tearn;
	}
	public void setTearn(String tearn) {
		this.tearn = tearn;
	}
	public String getDearn() {
		return dearn;
	}
	public void setDearn(String dearn) {
		this.dearn = dearn;
	}
	public String getAvailable() {
		return available;
	}
	public void setAvailable(String available) {
		this.available = available;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getFreez() {
		return freez;
	}
	public void setFreez(String freez) {
		this.freez = freez;
	}
	public String getIcounter() {
		return icounter;
	}
	public void setIcounter(String icounter) {
		this.icounter = icounter;
	}
	
}
