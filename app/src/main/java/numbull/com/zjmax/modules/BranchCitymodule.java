package numbull.com.zjmax.modules;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BranchCitymodule {
	
	@Expose
	@SerializedName("id")
	private String id = "";
	
	@Expose
	@SerializedName("name")
	private String name = "";
	
	@Expose
	@SerializedName("nodes")
	private List<BranchNodesmodule> nodes;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<BranchNodesmodule> getNodes() {
		return nodes;
	}

	public void setNodes(List<BranchNodesmodule> nodes) {
		this.nodes = nodes;
	}
	
	
}
