package numbull.com.zjmax.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsDetailmodule {
	
	@Expose
	@SerializedName("e")
	private String e;
	
	@Expose
	@SerializedName("title")
	private String title;
	
	@Expose
	@SerializedName("date")
	private String date;
	
	@Expose
	@SerializedName("content")
	private String content;
	
	@Expose
	@SerializedName("author")
	private String author;
	
	@Expose
	@SerializedName("source")
	private String source;
	
	public String getE() {
		return e;
	}
	public void setE(String e) {
		this.e = e;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
	
}
