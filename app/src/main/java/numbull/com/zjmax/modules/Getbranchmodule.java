package numbull.com.zjmax.modules;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Getbranchmodule {
	
	@Expose
	@SerializedName("e")
	private String e = "";
	
	@Expose
	@SerializedName("list")
	private List<BranchPrvmodule> list;

	public String getE() {
		return e;
	}

	public void setE(String e) {
		this.e = e;
	}

	public List<BranchPrvmodule> getList() {
		return list;
	}

	public void setList(List<BranchPrvmodule> list) {
		this.list = list;
	}
	
	
}
