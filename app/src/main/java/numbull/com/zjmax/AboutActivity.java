package numbull.com.zjmax;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;


public class AboutActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_about);

        findViewById(R.id.act_about_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AboutActivity.this.finish();
            }
        });
    }

}
