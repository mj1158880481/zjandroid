package numbull.com.zjmax;

import android.graphics.Bitmap;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.List;

import numbull.com.zjmax.api.ZJClient;
import numbull.com.zjmax.callback.ResponseCallback;
import numbull.com.zjmax.httpcore.RequestParams;
import numbull.com.zjmax.modules.TransferDetailmodule;
import numbull.com.zjmax.utils.ZjTagHandler;


public class TransferDetailActivity extends ActionBarActivity {
    private ImageView intro_img;
    private String id;
    private TextView title, rate, value, reterm, sday, price, price_detail, intro_memo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_transfer_detail);

        id = getIntent().getStringExtra("transfer_id");

        title  = (TextView)findViewById(R.id.act_transfer_detail_title);
        rate   = (TextView)findViewById(R.id.act_transfer_detail_rate);
        value  = (TextView)findViewById(R.id.act_transfer_detail_value);
        reterm = (TextView)findViewById(R.id.act_transfer_detail_reterm);
        sday   = (TextView)findViewById(R.id.act_transfer_detail_sday);
        price  = (TextView)findViewById(R.id.act_transfer_detail_price);
        price_detail = (TextView)findViewById(R.id.act_transfer_detail_price_detail);
        intro_memo = (TextView)findViewById(R.id.act_transfer_detail_intro_info);
        intro_img = (ImageView)findViewById(R.id.act_transfer_detail_intro_img);

        findViewById(R.id.act_transfer_detail_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransferDetailActivity.this.finish();
            }
        });

        if(!"".equals(id))
            LoadData(id);
    }

    private void LoadData(String id){
        RequestParams params = new RequestParams();
        params.put("a", "114");
        params.put("id", id);
        ZJClient.GetTransferDetail(new ResponseCallback<TransferDetailmodule>(){
            private static final String TAG = "ResponseCallback";

            public void onSuccess(TransferDetailmodule response) {
                Log.i(TAG, "onSuccess with object returned");
                if("0".equals(response.getE())){
                    DecimalFormat df  = new DecimalFormat("###.00");
                    title.setText(response.getProject() + " (转让)");
                    value.setText(df.format(Integer.parseInt(response.getValue()) * 0.01) + "元");
                    rate.setText(response.getRate() + "%");
                    reterm.setText(response.getRemain() + "天");
                    sday.setText(response.getSday());
                    price.setText(df.format(Integer.parseInt(response.getPrice()) * 0.01) + "");
                    price_detail.setText("元 =(项目价值" + df.format(Integer.parseInt(response.getValue()) * 0.01)
                            + "-" + df.format(Integer.parseInt(response.getDiscount()) * 0.01) + ")");
                    intro_memo.setText(Html.fromHtml(response.getMemo(), null, new ZjTagHandler()));

                    if(response.getImage() != null){
                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.ic_drawer)
                                .showImageForEmptyUri(R.drawable.ic_drawer)
                                .showImageOnFail(R.drawable.ic_drawer)
                                .bitmapConfig(Bitmap.Config.RGB_565)
                                .imageScaleType(ImageScaleType.NONE)
                                .build();
                        ImageLoader.getInstance().displayImage(response.getImage(), intro_img, options);
                    }
                }else{
                    Toast.makeText(TransferDetailActivity.this, "获取数据失败", Toast.LENGTH_SHORT).show();
                }
            }

            public void onSuccess(List<TransferDetailmodule> response) {
                Log.i(TAG, "onSuccess with object list returned: " + response.size());
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                Toast.makeText(TransferDetailActivity.this, "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                Toast.makeText(TransferDetailActivity.this, "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
            }

            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                Toast.makeText(TransferDetailActivity.this, "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
            }
        }, params);
    }

}
