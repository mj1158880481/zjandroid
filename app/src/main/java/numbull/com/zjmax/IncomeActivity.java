package numbull.com.zjmax;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.List;

import numbull.com.zjmax.api.ZJClient;
import numbull.com.zjmax.callback.ResponseCallback;
import numbull.com.zjmax.httpcore.RequestParams;
import numbull.com.zjmax.modules.GetIncomemodule;
import numbull.com.zjmax.modules.Incomemodule;


public class IncomeActivity extends ActionBarActivity {
    private ListView listview;
    private IncomeAdapter adapter;
    private int offset = 0;
    private int limit = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_income);

        listview = (ListView)findViewById(R.id.act_income_listview);
        findViewById(R.id.act_income_back).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                IncomeActivity.this.finish();
            }

        });

        LoadData(offset);

    }

    private void LoadData(int off){
        ZJApplication app = (ZJApplication)getApplication();
        RequestParams params = new RequestParams();
        params.put("a", "118");
        params.put("u", app.getUserinfo().getU());
        params.put("offset", off+"");
        params.put("limit", limit+"");
        ZJClient.GetIncomelist(new ResponseCallback<GetIncomemodule>() {
            private static final String TAG = "ResponseCallback";

            public void onSuccess(GetIncomemodule response) {
                Log.i(TAG, "onSuccess with object returned");
                if (offset == 0) {
                    if ("0".equals(response.getE())) {
                        adapter = new IncomeAdapter(IncomeActivity.this, 0, response.getList());
                        listview.setAdapter(adapter);
                    } else {
                        Toast.makeText(IncomeActivity.this, "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (adapter != null) {
                        for (int i = 0; i < response.getList().size(); i++) {
                            adapter.add(response.getList().get(i));
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
                offset = offset + response.getList().size();
            }

            public void onSuccess(List<GetIncomemodule> response) {
                Log.i(TAG, "onSuccess with object list returned: " + response.size());

            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());

                Toast.makeText(IncomeActivity.this, "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());

                Toast.makeText(IncomeActivity.this, "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
            }

            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());

                Toast.makeText(IncomeActivity.this, "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
            }
        }, params);
    }

    class IncomeAdapter extends ArrayAdapter<Incomemodule> {
        private Context context;
        private List<Incomemodule> list;

        public IncomeAdapter(Context context, int resource,
                             List<Incomemodule> list) {
            super(context, resource, list);
            // TODO Auto-generated constructor stub
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return list.size();
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return super.getItemId(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            ViewHolder holder = null;
            if(convertView == null){
                holder = new ViewHolder();
                convertView = View.inflate(context, R.layout.item_income_listview, null);
                holder.name = (TextView)convertView.findViewById(R.id.item_income_listview_name);
                holder.date = (TextView)convertView.findViewById(R.id.item_income_listview_date);
                holder.dearn = (TextView)convertView.findViewById(R.id.item_income_listview_already);
                holder.tearn = (TextView)convertView.findViewById(R.id.item_income_listview_will);
                holder.next = (TextView)convertView.findViewById(R.id.item_income_listview_next);
                holder.amount = (TextView)convertView.findViewById(R.id.item_income_listview_amount);
                holder.term = (TextView)convertView.findViewById(R.id.item_income_listview_term);
                holder.rate = (TextView)convertView.findViewById(R.id.item_income_listview_rate);

                convertView.setTag(holder);
            }else{
                holder = (ViewHolder)convertView.getTag();
            }

            holder.name.setText(list.get(position).getName());
            holder.date.setText(list.get(position).getDateline());
//            holder.dearn.setText(list.get(position).getDearn());
            holder.next.setText(list.get(position).getNearn());
            DecimalFormat df  = new DecimalFormat("###.00");
            if(!"".equals(list.get(position).getDearn()) && !"0".equals(list.get(position).getDearn()))
                holder.dearn.setText(df.format((Integer.parseInt(list.get(position).getDearn()) * 0.01)) + "");
            else
                holder.dearn.setText("0");

            if(!"".equals(list.get(position).getTearn()) && !"0".equals(list.get(position).getTearn()))
                holder.tearn.setText(df.format((Integer.parseInt(list.get(position).getTearn()) * 0.01)) + "");
            else
                holder.tearn.setText("0");
            holder.amount.setText("投资额" + Integer.parseInt(list.get(position).getAmount())/1000000 + "万");
            holder.term.setText("期限" + list.get(position).getTerm() + "天");
            holder.rate.setText("预期年化收益率" + list.get(position).getRate() + "%");

            return convertView;
        }

    }

    class ViewHolder {
        private TextView name;
        private TextView date;
        private TextView dearn;
        private TextView tearn;
        private TextView next;
        private TextView amount;
        private TextView term;
        private TextView rate;
    }

}
