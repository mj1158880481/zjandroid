package numbull.com.zjmax;


import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import numbull.com.zjmax.fragments.HomepageFragment;
import numbull.com.zjmax.utils.Constants;

public class WizardActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private HomepageFragment homepageFragment;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_wizard);

        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.wizard);
        toolbar.setTitle("");
        toolbar.setLogo(R.drawable.icon_main_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
                drawerLayout, toolbar);

        FragmentManager fragmentManager = getSupportFragmentManager();
        if(homepageFragment == null)
            homepageFragment = new HomepageFragment();
        fragmentManager.beginTransaction()
                .replace(R.id.container, homepageFragment)
                .commit();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        switch(position){
            case 0:
                WizardActivity.this.startActivity(new Intent(WizardActivity.this, RecordActivity.class));
                break;

            case 1:
                WizardActivity.this.startActivity(new Intent(WizardActivity.this, IncomeActivity.class));
                break;

            case 2:
                WizardActivity.this.startActivity(new Intent(WizardActivity.this, BranchActivity.class));
                break;

            case 3:
                break;

            case 4:
                WizardActivity.this.startActivity(new Intent(WizardActivity.this, SetActivity.class));
                break;

            case 5:
                WizardActivity.this.startActivity(new Intent(WizardActivity.this, AboutActivity.class));
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.wizard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_news) {
            Intent intent = new Intent(WizardActivity.this, NewsActivity.class);
            WizardActivity.this.startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if(Constants.isdrawropen){
            drawerLayout.closeDrawer(Gravity.LEFT);
            Constants.isdrawropen = false;
        }else{
            exitBy2Click();
        }
    }

    /**
     * 双击退出函数
     */
    private static Boolean isExit = false;

    private void exitBy2Click() {
        Timer tExit = null;
        if (isExit == false) {
            isExit = true;
            Toast.makeText(this, "再按一次退出", Toast.LENGTH_SHORT).show();
            tExit = new Timer();
            tExit.schedule(new TimerTask() {
                @Override
                public void run() {
                    isExit = false;
                }
            }, 2000);
        } else {
            finish();
            System.exit(0);
        }
    }

}
