package numbull.com.zjmax.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import numbull.com.zjmax.R;


public class HomepageFragment extends Fragment implements ViewPager.OnPageChangeListener, View.OnClickListener{
    private View rootView;
    private ViewPager viewPager;
    private TextView  title2, title3;
    private View tab2, tab3;
    private ProductlistFragment f1;
    private TransferlistFragment f2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_homepage, container, false);

            title2 = (TextView)rootView.findViewById(R.id.fragment_homepage_tv2);
            title3 = (TextView)rootView.findViewById(R.id.fragment_homepage_tv3);
            tab2 = (View)rootView.findViewById(R.id.tab2);
            tab3 = (View)rootView.findViewById(R.id.tab3);
            title2.setOnClickListener(this);
            title3.setOnClickListener(this);

            viewPager = (ViewPager)rootView.findViewById(R.id.fragment_homepage_viewpager);
            List<Fragment> fragments = new ArrayList<Fragment>();
            f1 = new ProductlistFragment();
            f2 = new TransferlistFragment();
            fragments.add(f1);
            fragments.add(f2);
            viewPager.setAdapter(new Adapter(getChildFragmentManager(), fragments));
            viewPager.setCurrentItem(0);
            viewPager.setOnPageChangeListener(this);

            changesections(0);
        }
        ViewGroup parent = (ViewGroup)rootView.getParent();
        if(parent != null){
            parent.removeView(rootView);
        }

        return rootView;
    }

    private void changesections(int position){
        switch (position){
            case 0:
                title2.setTextColor(getResources().getColor(R.color.sections_sele));
                title3.setTextColor(getResources().getColor(R.color.sections_normal));
                tab2.setVisibility(View.VISIBLE);
                tab3.setVisibility(View.INVISIBLE);
                break;

            case 1:
                title2.setTextColor(getResources().getColor(R.color.sections_normal));
                title3.setTextColor(getResources().getColor(R.color.sections_sele));
                tab2.setVisibility(View.INVISIBLE);
                tab3.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        changesections(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fragment_homepage_tv2:
                changesections(0);
                viewPager.setCurrentItem(0);
                break;

            case R.id.fragment_homepage_tv3:
                changesections(1);
                viewPager.setCurrentItem(1);
                break;
        }
    }

    class Adapter extends FragmentPagerAdapter {
        private List<Fragment> fragments;

        public Adapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

//        @Override
//        public Fragment getItem(int position) {
//            if(position == 0){
//                if(f1 == null)
//                    f1  =new ProductlistFragment();
//
//                return f1;
//            }else{
//                if(f2 == null)
//                    f2  =new TransferlistFragment();
//
//                return f2;
//            }
//        }

    }
}
