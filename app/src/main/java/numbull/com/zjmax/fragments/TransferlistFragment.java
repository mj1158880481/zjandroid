package numbull.com.zjmax.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.List;

import numbull.com.zjmax.R;
import numbull.com.zjmax.TransferDetailActivity;
import numbull.com.zjmax.api.ZJClient;
import numbull.com.zjmax.callback.ResponseCallback;
import numbull.com.zjmax.httpcore.RequestParams;
import numbull.com.zjmax.modules.GetTransfermodule;
import numbull.com.zjmax.modules.Transfermodule;

/**
 * Created by leo on 2015/3/17.
 */
public class TransferlistFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private View rootView;
    private ListView listview;
    private SwipeRefreshLayout refresh;
    private TransferAdapter adapter;
    private int num = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_transferlist, container, false);
            refresh = (SwipeRefreshLayout)rootView.findViewById(R.id.fragment_transferlist_refresh);
            refresh.setColorScheme(R.color.refresh_color, R.color.refresh_color_sec);
            refresh.setOnRefreshListener(this);
            listview = (ListView)rootView.findViewById(R.id.fragment_transferlist_listview);
            listview.setDivider(null);
            listview.setOnScrollListener(new AbsListView.OnScrollListener(){

                @Override
                public void onScrollStateChanged(AbsListView view,
                                                 int scrollState) {
                    // TODO Auto-generated method stub
                    switch(scrollState){
                        case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                            if(view.getLastVisiblePosition() > (view.getCount() - 2)){
                                LoadData(num);
                            }
                            break;
                    }
                }

                @Override
                public void onScroll(AbsListView view,
                                     int firstVisibleItem, int visibleItemCount,
                                     int totalItemCount) {
                    // TODO Auto-generated method stub

                }

            });

            LoadData(num);
        }
        ViewGroup parent = (ViewGroup)rootView.getParent();
        if(parent != null){
            parent.removeView(rootView);
        }

        return rootView;
    }

    private void LoadData(int offset){
        Log.i("startload", "transfer");
        refresh.setRefreshing(true);
        RequestParams params = new RequestParams();
        params.put("a", "108");
        params.put("t", "1");
        params.put("offset", offset + "");
        params.put("limit", "10");
        ZJClient.GetTransferList(new ResponseCallback<GetTransfermodule>(){
            private static final String TAG = "ResponseCallback";

            public void onSuccess(final GetTransfermodule response) {
//                Log.i(TAG, "onSuccess with object returned");
                refresh.setRefreshing(false);
                if("0".equals(response.getE())){
                    if(num == 0){
                        adapter = new TransferAdapter(getActivity(), 0, response.getList());
                        listview.setAdapter(adapter);
                        listview.setOnItemClickListener(new AdapterView.OnItemClickListener(){

                            @Override
                            public void onItemClick(AdapterView<?> parent,
                                                    View view, int position, long id) {
                                // TODO Auto-generated method stub
                                Intent intent = new Intent(getActivity(), TransferDetailActivity.class);
                                intent.putExtra("transfer_id", response.getList().get(position).getId());
                                getActivity().startActivity(intent);
                            }

                        });
                    }else{
                        if(adapter != null){
                            for(int i=0;i>response.getList().size();i++){
                                adapter.add(response.getList().get(i));
                            }
                            adapter.notifyDataSetChanged();
                        }
                    }
                    num = num + response.getList().size();
                }else{
                    Toast.makeText(getActivity(), "获取数据失败", Toast.LENGTH_SHORT).show();
                }
            }

            public void onSuccess(List<GetTransfermodule> response) {
                Log.i(TAG, "onSuccess with object list returned: " + response.size());
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                refresh.setRefreshing(false);
                Toast.makeText(getActivity(), "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                refresh.setRefreshing(false);
                Toast.makeText(getActivity(), "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
            }

            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                refresh.setRefreshing(false);
                Toast.makeText(getActivity(), "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
            }
        }, params);
    }

    @Override
    public void onRefresh() {
        num = 0;
        LoadData(num);
    }

    private class TransferAdapter extends ArrayAdapter<Transfermodule> {
        private List<Transfermodule> list;

        public TransferAdapter(Context context, int resource, List<Transfermodule> list) {
            super(context, resource, list);
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if(convertView == null){
                holder = new ViewHolder();
                convertView = View.inflate(getActivity(), R.layout.item_transferlist, null);

                holder.rate = (TextView)convertView.findViewById(R.id.item_transferlist_rate);
                holder.reterm = (TextView)convertView.findViewById(R.id.item_transferlist_reterm);
                holder.price = (TextView)convertView.findViewById(R.id.item_transferlist_price);
                holder.name = (TextView)convertView.findViewById(R.id.item_transferlist_name);

                convertView.setTag(holder);
            }else{
                holder = (ViewHolder)convertView.getTag();
            }

            holder.rate.setText(list.get(position).getRate());
            holder.reterm.setText(list.get(position).getRemain());
            holder.name.setText(list.get(position).getProject());
            DecimalFormat df  = new DecimalFormat("###.00");
            holder.price.setText(df.format(Integer.parseInt(list.get(position).getPrice()) * 0.01) + "");

            return convertView;
        }
    }

    private class ViewHolder {
        private TextView rate;
        private TextView reterm;
        private TextView price;
        private TextView name;
    }
}
