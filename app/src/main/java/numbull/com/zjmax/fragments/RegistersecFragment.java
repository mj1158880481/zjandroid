package numbull.com.zjmax.fragments;

import java.lang.ref.WeakReference;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import numbull.com.zjmax.LoginActivity;
import numbull.com.zjmax.R;
import numbull.com.zjmax.ZJApplication;
import numbull.com.zjmax.api.ZJClient;
import numbull.com.zjmax.callback.ResponseCallback;
import numbull.com.zjmax.httpcore.RequestParams;
import numbull.com.zjmax.modules.CheckNummodule;
import numbull.com.zjmax.modules.Loginmodule;
import numbull.com.zjmax.modules.Registerbackmodule;
import numbull.com.zjmax.utils.Constants;

public class RegistersecFragment extends Fragment {
	private View rootview;
	public TextView sendnum;
	private EditText username, num, password, repassword;
	private MyHandler handler;
	private int count = 60;
	boolean running = false;
	
	static class MyHandler extends Handler implements OnClickListener{
		WeakReference<RegistersecFragment> mfragment;
		
		public MyHandler(RegistersecFragment fragment){
			mfragment = new WeakReference<RegistersecFragment>(fragment);
		}
		
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			final RegistersecFragment mf = mfragment.get();
			super.handleMessage(msg);
			switch(msg.what){
			case 0:
				mf.sendnum.setText("发送");
				mf.count = 60;
				break;
				
			case 1:
				mf.sendnum.setText(mf.count + "秒");
				break;
			}
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if(rootview == null){
			handler = new MyHandler(this);
			rootview = inflater.inflate(R.layout.fragment_register_sec, container, false);
			
			username = (EditText)rootview.findViewById(R.id.fragment_register_sec_phone);
			num = (EditText)rootview.findViewById(R.id.fragment_register_sec_num);
			password = (EditText)rootview.findViewById(R.id.fragment_register_sec_password);
			repassword = (EditText)rootview.findViewById(R.id.fragment_register_sec_repassword);
			sendnum = (TextView)rootview.findViewById(R.id.fragment_register_sec_sendnum);
			
			sendnum.setOnClickListener(
					new OnClickListener(){

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if(!"".equals(username.getText().toString())){
								if(running){

								}else{
									
									SendChecknum();

								}
							}else{
								Toast.makeText(getActivity(), "请先填写手机号", Toast.LENGTH_SHORT).show();
							}
							
						}
						
					});
			
			rootview.findViewById(R.id.fragment_register_sec_finish).setOnClickListener(
					new OnClickListener(){

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if("".equals(username.getText().toString())){
								Toast.makeText(getActivity(), "请填写手机号", Toast.LENGTH_SHORT).show();
							}else if("".equals(num.getText().toString())){
								Toast.makeText(getActivity(), "请填写验证码", Toast.LENGTH_SHORT).show();
							}else if("".equals(password.getText().toString())){
								Toast.makeText(getActivity(), "请填写密码", Toast.LENGTH_SHORT).show();
							}else if(password.getText().toString().toCharArray().length < 8){
								Toast.makeText(getActivity(), "为了您的账户安全，密码长度不能小于8位", Toast.LENGTH_SHORT).show();
							}else if("".equals(repassword.getText().toString())){
								Toast.makeText(getActivity(), "请再填写一次密码", Toast.LENGTH_SHORT).show();
							}else if(!password.getText().toString().equals(repassword.getText().toString())){
								Toast.makeText(getActivity(), "两次密码不一致，请重新填写", Toast.LENGTH_SHORT).show();
							}else{
								DoRegister();
							}
						}
						
					});
			
		}
		
		ViewGroup parent = (ViewGroup)rootview.getParent();
		if(parent != null){
			parent.removeView(rootview);
		}
		
		return rootview;
	}
	
	private void DoRegister(){
		RequestParams params = new RequestParams();
		params.put("a", "104");
		params.put("mobile", username.getText().toString());
		params.put("code", num.getText().toString());
		params.put("pass", password.getText().toString());
		params.put("ru", Constants.recomd);
		params.put("province", Constants.prv_id);
		params.put("city", Constants.city_id);
		params.put("node", Constants.branch_id);
		ZJClient.Register(new ResponseCallback<Registerbackmodule>() {
            private static final String TAG = "ResponseCallback";

            public void onSuccess(Registerbackmodule response) {
                Log.i(TAG, "onSuccess with object returned");
                if ("0".equals(response.getE())) {
                    Toast.makeText(getActivity(), "注册成功", Toast.LENGTH_SHORT).show();
                    Login();
                } else if ("308".equals(response.getE())) {
                    Toast.makeText(getActivity(), "手机号码不合法", Toast.LENGTH_SHORT).show();
                } else if ("309".equals(response.getE())) {
                    Toast.makeText(getActivity(), "该手机号已被注册", Toast.LENGTH_SHORT).show();
                } else if ("310".equals(response.getE())) {
                    Toast.makeText(getActivity(), "验证码错误", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "error" + response.getE(), Toast.LENGTH_SHORT).show();
                }
            }

            public void onSuccess(List<Registerbackmodule> response) {
                Log.i(TAG, "onSuccess with object list returned: " + response.size());
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                Toast.makeText(getActivity(), "网络连接不畅，请检查网络", Toast.LENGTH_SHORT).show();
                getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                Toast.makeText(getActivity(), "网络连接不畅，请检查网络", Toast.LENGTH_SHORT).show();
                getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
            }

            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                Toast.makeText(getActivity(), "网络连接不畅，请检查网络", Toast.LENGTH_SHORT).show();
                getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
            }
        }, params);
	}
	
	private void SendChecknum() {
		if(!"".equals(username.getText().toString())){
			RequestParams params = new RequestParams();
			params.put("a", "102");
			params.put("mobile", username.getText().toString());
			ZJClient.sendChecknum(new ResponseCallback<CheckNummodule>(){
				private static final String TAG = "ResponseCallback";
	
				public void onSuccess(CheckNummodule response) {
					Log.i(TAG, "onSuccess with object returned");
					if("0".equals(response.getE())){
                        final Runnable r = new Runnable() {
                            public void run() {
                                running = true;
                                while(count > 0) {
                                    count = count - 1;
                                    handler.sendEmptyMessage(1);
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                }
                                running = false;
                                handler.sendEmptyMessage(0);
                            }
                        };
                        Thread s = new Thread(r);
                        s.start();
					}else{
						Toast.makeText(getActivity(), "error" + response.getE(), Toast.LENGTH_SHORT).show();
					}
				}
	
				public void onSuccess(List<CheckNummodule> response) {
					Log.i(TAG, "onSuccess with object list returned: " + response.size());
				}
				
				public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
					Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
							+ throwable.getMessage());
					Toast.makeText(getActivity(), "网络连接不畅，请检查网络", Toast.LENGTH_SHORT).show();
				}
	
				public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
					Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
							+ throwable.getMessage());
					Toast.makeText(getActivity(), "网络连接不畅，请检查网络", Toast.LENGTH_SHORT).show();
				}
				
				public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
					Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
							+ throwable.getMessage());
					Toast.makeText(getActivity(), "网络连接不畅，请检查网络", Toast.LENGTH_SHORT).show();
				}
			}, params);
		}else{
			Toast.makeText(getActivity(), "请先填写手机号", Toast.LENGTH_SHORT).show();
		}
	}

    private void Login(){
        RequestParams params = new RequestParams();
        params.put("a", "100");
        params.put("account", username.getText().toString());
        params.put("password", password.getText().toString());
        ZJClient.Login(new ResponseCallback<Loginmodule>() {
            private static final String TAG = "ResponseCallback";

            public void onSuccess(Loginmodule response) {
                Log.i(TAG, "onSuccess with object returned");
                if ("0".equals(response.getE())) {
                    Constants.u = response.getU();
                    ZJApplication zjapp = (ZJApplication)getActivity().getApplication();
                    zjapp.setUserinfo(response);
                    getActivity().finish();
                }
            }

            public void onSuccess(List<Loginmodule> response) {
                Log.i(TAG, "onSuccess with object list returned: " + response.size());

            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());

            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());

            }

            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());

            }
        }, params);
    }

}
