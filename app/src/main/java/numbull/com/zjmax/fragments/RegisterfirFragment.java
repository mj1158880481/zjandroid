package numbull.com.zjmax.fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import numbull.com.zjmax.R;
import numbull.com.zjmax.ZJApplication;
import numbull.com.zjmax.api.ZJClient;
import numbull.com.zjmax.callback.ResponseCallback;
import numbull.com.zjmax.httpcore.RequestParams;
import numbull.com.zjmax.modules.Getbranchmodule;
import numbull.com.zjmax.utils.Constants;
import numbull.com.zjmax.widgets.Dialogbranch;

@SuppressLint("ValidFragment")
public class RegisterfirFragment extends Fragment implements OnClickListener{
	private ZJApplication app;
    private Dialogbranch dialog;
	private View rootview;
	private Button next;
	private Changesecfrag sfg;
	private RelativeLayout prv, city, branch;
	private EditText recomd;
	private TextView prv_tv, city_tv, branch_tv;
	
	public RegisterfirFragment(ZJApplication app){
		this.app = app;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if(rootview == null){
			rootview = inflater.inflate(R.layout.fragment_register_fir, container, false);
			
			next = (Button)rootview.findViewById(R.id.act_register_fir_next);
			prv  = (RelativeLayout)rootview.findViewById(R.id.act_register_fir_prv);
			city = (RelativeLayout)rootview.findViewById(R.id.act_register_fir_city);
			branch = (RelativeLayout)rootview.findViewById(R.id.act_register_fir_branch);
			prv_tv  = (TextView)rootview.findViewById(R.id.act_register_fir_prv_tv);
			city_tv = (TextView)rootview.findViewById(R.id.act_register_fir_city_tv);
			branch_tv = (TextView)rootview.findViewById(R.id.act_register_fir_branch_tv);
			recomd = (EditText)rootview.findViewById(R.id.act_register_fir_reco);
			
			prv.setOnClickListener(this);
			city.setOnClickListener(this);
			branch.setOnClickListener(this);
			
			next.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if("".equals(Constants.prv) || "".equals(Constants.city) || "".equals(Constants.branch)){
						Toast.makeText(getActivity(), "请选择服务您的网点", Toast.LENGTH_SHORT).show();
					}else{
						Constants.recomd = recomd.getText().toString();
						sfg.changesecfragment();
					}
				}
				
			});
		}
		
		ViewGroup parent = (ViewGroup)rootview.getParent();
		if(parent != null){
			parent.removeView(rootview);
		}
		
		return rootview;
	}
	
	public void setchangelistener(Changesecfrag sfg){
		this.sfg = sfg;
	}
	
	public interface Changesecfrag{
		void changesecfragment();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.act_register_fir_prv:
            if(app.getBranchs() == null) {
                Loadbranchs();
            }else{
                if(dialog == null){
                    dialog = new Dialogbranch(getActivity(), R.style.dialog, new Dialogbranch.UpdateInfo(){

                        @Override
                        public void UpdateUI() {
                            // TODO Auto-generated method stub
                            prv_tv.setText(Constants.prv);
                            city_tv.setText(Constants.city);
                            branch_tv.setText(Constants.branch);
                        }

                    }, app);
                }
                dialog.setTitle("选择服务您的网点");
                dialog.show();
            }
			break;
			
		case R.id.act_register_fir_city:
            if(app.getBranchs() == null) {
                Loadbranchs();
            }else{
                if(dialog == null){
                    dialog = new Dialogbranch(getActivity(), R.style.dialog, new Dialogbranch.UpdateInfo(){

                        @Override
                        public void UpdateUI() {
                            // TODO Auto-generated method stub
                            prv_tv.setText(Constants.prv);
                            city_tv.setText(Constants.city);
                            branch_tv.setText(Constants.branch);
                        }

                    }, app);
                }
                dialog.setTitle("选择服务您的网点");
                dialog.show();
            }
			break;
			
		case R.id.act_register_fir_branch:
            if(app.getBranchs() == null) {
                Loadbranchs();
            }else{
                if(dialog == null){
                    dialog = new Dialogbranch(getActivity(), R.style.dialog, new Dialogbranch.UpdateInfo(){

                        @Override
                        public void UpdateUI() {
                            // TODO Auto-generated method stub
                            prv_tv.setText(Constants.prv);
                            city_tv.setText(Constants.city);
                            branch_tv.setText(Constants.branch);
                        }

                    }, app);
                }
                dialog.setTitle("选择服务您的网点");
                dialog.show();
            }
			break;
		}
	}

    public void Loadbranchs(){
        final ProgressDialog pd = ProgressDialog.show(getActivity(), "正在获取网点列表", "");
        RequestParams params = new RequestParams();
        params.put("a", "111");
        params.put("u", "0");
        ZJClient.GetBranch(new ResponseCallback<Getbranchmodule>() {
            private static final String TAG = "ResponseCallback";

            public void onSuccess(Getbranchmodule response) {
                Log.i(TAG, "onSuccess with object returned");
                if(pd != null){
                    pd.dismiss();
                }
                if ("0".equals(response.getE())) {
                    ZJApplication zjapp = (ZJApplication) getActivity().getApplication();
                    zjapp.setBranchs(response);
                    if(dialog == null){
                        dialog = new Dialogbranch(getActivity(), R.style.dialog, new Dialogbranch.UpdateInfo(){

                            @Override
                            public void UpdateUI() {
                                // TODO Auto-generated method stub
                                prv_tv.setText(Constants.prv);
                                city_tv.setText(Constants.city);
                                branch_tv.setText(Constants.branch);
                            }

                        }, app);
                    }
                    dialog.setTitle("选择服务您的网点");
                    dialog.show();
                } else {
                    Toast.makeText(getActivity(), "网络不给力，请检查网络", Toast.LENGTH_SHORT).show();
                }
            }

            public void onSuccess(List<Getbranchmodule> response) {
                Log.i(TAG, "onSuccess with object list returned: " + response.size());
                if(pd != null){
                    pd.dismiss();
                }
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                if(pd != null){
                    pd.dismiss();
                }
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                if(pd != null){
                    pd.dismiss();
                }
            }

            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                if(pd != null){
                    pd.dismiss();
                }
            }
        }, params);
    }
}
