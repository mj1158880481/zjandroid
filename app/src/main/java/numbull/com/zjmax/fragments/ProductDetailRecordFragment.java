package numbull.com.zjmax.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import numbull.com.zjmax.R;
import numbull.com.zjmax.adapters.ProductDetailRecordAdapter;
import numbull.com.zjmax.modules.ProductRecord;

/**
 * Created by leo on 2015/3/18.
 */
@SuppressLint("ValidFragment")
public class ProductDetailRecordFragment extends Fragment{
    private View rootview;
    private LinearLayout body;
    private List<ProductRecord> list;

    public ProductDetailRecordFragment(List<ProductRecord> list){
        this.list = list;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        if(rootview == null){
            rootview = View.inflate(getActivity(), R.layout.fragment_productdetail_record, null);
            body = (LinearLayout)rootview.findViewById(R.id.fragment_productdetail_record_listview);

            if(list != null){
                for(int i=0;i<list.size();i++){
                    View view = View.inflate(getActivity(), R.layout.item_productdetail_record, null);
                    TextView user = (TextView)view.findViewById(R.id.item_productdetail_record_user);
                    TextView sum  = (TextView)view.findViewById(R.id.item_productdetail_record_sum);
                    TextView data = (TextView)view.findViewById(R.id.item_productdetail_record_data);
                    user.setText(list.get(i).getName());
                    sum.setText(list.get(i).getAmount());
                    data.setText(list.get(i).getOccur());
                    body.addView(view);
                }
            }
        }

        ViewGroup parent = (ViewGroup)rootview.getParent();
        if(parent != null){
            parent.removeView(rootview);
        }

        return rootview;
    }

}
