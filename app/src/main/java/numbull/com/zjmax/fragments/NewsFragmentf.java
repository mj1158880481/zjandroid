package numbull.com.zjmax.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import numbull.com.zjmax.NewsDetailActivity;
import numbull.com.zjmax.R;
import numbull.com.zjmax.api.ZJClient;
import numbull.com.zjmax.callback.ResponseCallback;
import numbull.com.zjmax.httpcore.RequestParams;
import numbull.com.zjmax.modules.Newsitemmodule;
import numbull.com.zjmax.modules.Newsmodule;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NewsFragmentf#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewsFragmentf extends Fragment {
    private Context context;
    private View rootview;
    private int offset = 0;
    private int limit = 15;
    private Adapter adapter = null;
    private ListView listview;

    // TODO: Rename and change types and number of parameters
    public static NewsFragmentf newInstance() {
        NewsFragmentf fragment = new NewsFragmentf();
        return fragment;
    }

    public NewsFragmentf() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        if(rootview == null){
            rootview = inflater.inflate(R.layout.fragment_newsf, container, false);
            listview = (ListView)rootview.findViewById(R.id.fragment_newsf_listview);

            listview.setOnScrollListener(new AbsListView.OnScrollListener(){

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                    // TODO Auto-generated method stub
                    switch(scrollState){
                        case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                            if(view.getLastVisiblePosition() > (view.getCount() - 5)){
                                loadmore();
                            }
                            break;
                    }
                }
                @Override
                public void onScroll(AbsListView view, int firstVisibleItem,
                                     int visibleItemCount, int totalItemCount) {
                    // TODO Auto-generated method stub

                }

            });

            RequestParams params = new RequestParams();
            params.put("a", "122");
            params.put("type", "1");
            params.put("offset", offset + "");
            params.put("limit", limit + "");
            ZJClient.GetNews(new ResponseCallback<Newsmodule>(){
                private static final String TAG = "ResponseCallback";
                public void onSuccess(final Newsmodule response) {
                    Log.i(TAG, "onSuccess with object returned");
                    if(response.getE().equals("0")){
                        offset = offset + response.getList().size();
                        adapter = new Adapter(context, 0, response.getList());
                        listview.setAdapter(adapter);
                        listview.setOnItemClickListener(new AdapterView.OnItemClickListener(){

                            @Override
                            public void onItemClick(AdapterView<?> parent,
                                                    View view, int position, long id) {
                                // TODO Auto-generated method stub
                                TextView title = (TextView)view.findViewById(R.id.item_news_listview_title);
                                TextView data  = (TextView)view.findViewById(R.id.item_news_listview_data);
                                title.setTextColor(getResources().getColor(R.color.text_label1));
                                data.setTextColor(getResources().getColor(R.color.text_label1));
                                Intent intent = new Intent(context, NewsDetailActivity.class);
                                intent.putExtra("news_id", response.getList().get(position).getId());
                                context.startActivity(intent);
                            }

                        });

                    }else{
                        Toast.makeText(context, "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
                    }
                }

                public void onSuccess(List<Newsmodule> response) {
                    Log.i(TAG, "onSuccess with object list returned: " + response.size());
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                            + throwable.getMessage());;
                    Toast.makeText(context, "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                            + throwable.getMessage());
                    Toast.makeText(context, "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
                }

                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                            + throwable.getMessage());
                    Toast.makeText(context, "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
                }
            }, params);
        }

        ViewGroup parent = (ViewGroup)rootview.getParent();
        if(parent != null){
            parent.removeView(rootview);
        }

        return rootview;
    }

    public void loadmore(){
        RequestParams paramss = new RequestParams();
        paramss.put("a", "122");
//		paramss.put("u", Constants.u);
        paramss.put("type", "1");
        paramss.put("offset", offset + "");
        paramss.put("limit", limit + "");
        ZJClient.GetNews(new ResponseCallback<Newsmodule>() {
            private static final String TAG = "ResponseCallback";

            public void onSuccess(Newsmodule response) {
                Log.i(TAG, "onSuccess with object returned");
                if (response.getE().equals("0")) {
                    offset = offset + response.getList().size();
                    for (int i = 0; i < response.getList().size(); i++) {
                        adapter.add(response.getList().get(i));
                    }
                    adapter.notifyDataSetChanged();
                }
            }

            public void onSuccess(List<Newsmodule> response) {
                Log.i(TAG, "onSuccess with object list returned: " + response.size());
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
            }

            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
            }
        }, paramss);
    }

    class Adapter extends ArrayAdapter<Newsitemmodule> {
        private Context context;
        private List<Newsitemmodule> list;

        public Adapter(Context context, int resource,
                       List<Newsitemmodule> list) {
            super(context, resource, list);
            // TODO Auto-generated constructor stub
            this.context = context;
            this.list = list;
        }

        @Override
        public void add(Newsitemmodule object) {
            // TODO Auto-generated method stub
            super.add(object);
        }

        @Override
        public void notifyDataSetChanged() {
            // TODO Auto-generated method stub
            super.notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return list.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            ViewHolder holder = null;
            if(convertView == null){
                holder = new ViewHolder();
                convertView = View.inflate(context, R.layout.item_news_listview, null);
                holder.title = (TextView)convertView.findViewById(R.id.item_news_listview_title);
                holder.data  = (TextView)convertView.findViewById(R.id.item_news_listview_data);

                convertView.setTag(holder);
            }else{
                holder = (ViewHolder)convertView.getTag();
            }
            String temp = list.get(position).getTitle();
            char[] temps = temp.toCharArray();
            if(temps.length > 15){
                temp = temp.substring(0, 15);
            }
            holder.title.setText(temp + "...");
            holder.data.setText(list.get(position).getDate());

            return convertView;
        }

    }

    private class ViewHolder{
        private TextView title;
        private TextView data;
    }


}
