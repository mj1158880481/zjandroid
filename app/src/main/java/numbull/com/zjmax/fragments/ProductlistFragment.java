package numbull.com.zjmax.fragments;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import numbull.com.zjmax.ProductDetailActivity;
import numbull.com.zjmax.R;
import numbull.com.zjmax.api.ZJClient;
import numbull.com.zjmax.callback.ResponseCallback;
import numbull.com.zjmax.httpcore.RequestParams;
import numbull.com.zjmax.modules.GetProductmodule;
import numbull.com.zjmax.modules.Productmodule;
import numbull.com.zjmax.widgets.ArcProgress;
import numbull.com.zjmax.widgets.CircleLabel;

/**
 * Created by leo on 2015/3/16.
 */
public class ProductlistFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View rootView;
    private ListView listview;
    private SwipeRefreshLayout refresh;
    private ListAdapter adapter;
    private int num = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_productlist, container, false);
            refresh = (SwipeRefreshLayout)rootView.findViewById(R.id.fragment_productlist_refresh);
            refresh.setColorScheme(R.color.refresh_color, R.color.refresh_color_sec);
            refresh.setOnRefreshListener(this);
            listview = (ListView)rootView.findViewById(R.id.fragment_productlist_listview);
            listview.setDivider(null);
            listview.setOnScrollListener(new AbsListView.OnScrollListener(){

                @Override
                public void onScrollStateChanged(AbsListView view,
                                                 int scrollState) {
                    // TODO Auto-generated method stub
                    switch(scrollState){
                        case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                            if(view.getLastVisiblePosition() > (view.getCount() - 2)){
                                LoadData(num);
                            }
                            break;
                    }
                }

                @Override
                public void onScroll(AbsListView view,
                                     int firstVisibleItem, int visibleItemCount,
                                     int totalItemCount) {
                    // TODO Auto-generated method stub

                }

            });

            LoadData(num);
        }
        ViewGroup parent = (ViewGroup)rootView.getParent();
        if(parent != null){
            parent.removeView(rootView);
        }

        return rootView;
    }

    private void LoadData(int offset){
        Log.i("startload", "product");
        refresh.setRefreshing(true);
        RequestParams params = new RequestParams();
        params.put("a", "108");
        params.put("t", "0");
        params.put("offset", offset + "");
        params.put("limit", "10");
        ZJClient.GetProductList(new ResponseCallback<GetProductmodule>(){
            private static final String TAG = "ResponseCallback";

            public void onSuccess(final GetProductmodule response) {
//                Log.i(TAG, "onSuccess with object returned");
                refresh.setRefreshing(false);
                if("0".equals(response.getE())){
                    if(num == 0){
                        adapter = new ListAdapter(getActivity(), 0, response.getList());
                        listview.setAdapter(adapter);
                        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent intent = new Intent(getActivity(), ProductDetailActivity.class);
                                intent.putExtra("product_id", response.getList().get(position).getId());
                                intent.putExtra("product_project", response.getList().get(position).getProject());
                                intent.putExtra("product_rate", response.getList().get(position).getRate());
                                intent.putExtra("product_min", response.getList().get(position).getMin());
                                intent.putExtra("product_term", response.getList().get(position).getTerm());
                                intent.putExtra("product_max", response.getList().get(position).getAvailable());
                                intent.putExtra("product_progress", response.getList().get(position).getProgress());
                                intent.putExtra("product_remain", response.getList().get(position).getRemain());
                                intent.putExtra("product_img", response.getList().get(position).getImg());
                                getActivity().startActivity(intent);
                            }
                        });
                    }else{
                        if(adapter != null){
                            for(int i=0;i<response.getList().size();i++){
                                adapter.add(response.getList().get(i));
                            }
                            adapter.notifyDataSetChanged();
                        }
                    }
                    num = num + response.getList().size();

                }else{
                    Toast.makeText(getActivity(), "获取数据失败", Toast.LENGTH_SHORT).show();
                }
            }

            public void onSuccess(List<GetProductmodule> response) {
                Log.i(TAG, "onSuccess with object list returned: " + response.size());
                refresh.setRefreshing(false);
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                refresh.setRefreshing(false);
                Toast.makeText(getActivity(), "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                refresh.setRefreshing(false);
                Toast.makeText(getActivity(), "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
            }

            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
                refresh.setRefreshing(false);
                Toast.makeText(getActivity(), "获取数据失败，请检查网络", Toast.LENGTH_SHORT).show();
            }
        }, params);
    }

    @Override
    public void onRefresh() {
        num = 0;
        LoadData(num);
    }

    private class ListAdapter extends ArrayAdapter<Productmodule> {
        private List<Productmodule> list;

        public ListAdapter(Context context, int resource, List<Productmodule> list) {
            super(context, resource, list);
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if(convertView == null){
                holder = new ViewHolder();
                convertView = View.inflate(getActivity(), R.layout.item_productlist, null);

                holder.rate = (TextView)convertView.findViewById(R.id.item_productlist_rate);
                holder.term = (TextView)convertView.findViewById(R.id.item_productlist_term);
                holder.min  = (TextView)convertView.findViewById(R.id.item_productlist_min);
                holder.day = (TextView)convertView.findViewById(R.id.item_productlist_term_day);
                holder.min_yuan = (TextView)convertView.findViewById(R.id.item_productlist_min_yuan);
                holder.name = (TextView)convertView.findViewById(R.id.item_productlist_name);
                holder.label = (TextView)convertView.findViewById(R.id.item_productlist_label);
                holder.clabel = (CircleLabel)convertView.findViewById(R.id.item_productlist_circlelabel);
                holder.aclabel = (CircleLabel)convertView.findViewById(R.id.item_productlist_aclabel);
                holder.progress = (ArcProgress)convertView.findViewById(R.id.item_productlist_progress);
                holder.status_img = (ImageView)convertView.findViewById(R.id.item_productlist_status_img);

                convertView.setTag(holder);
            }else{
                holder = (ViewHolder)convertView.getTag();
            }

            holder.rate.setText(list.get(position).getRate());
            holder.term.setText(list.get(position).getTerm());
            if(!"".equals(list.get(position).getMin())){
                String minx = list.get(position).getMin();
                char[] mincs = minx.toCharArray();
                if(mincs.length < 7){
                    holder.min.setText(Integer.parseInt(list.get(position).getMin()) / 100 + "");
                    holder.min_yuan.setText("元");
                }else{
                    holder.min.setText(Integer.parseInt(list.get(position).getMin()) / 1000000 + "");
                    holder.min_yuan.setText("万元");
                }
            }
            holder.name.setText(list.get(position).getProject());
            holder.label.setText(list.get(position).getSeries());
            if("2".equals(list.get(position).getStatus())){
                int color = getResources().getColor(R.color.text_label1);
                holder.rate.setTextColor(color);
                holder.term.setTextColor(color);
                holder.min.setTextColor(color);
                holder.day.setTextColor(color);
                holder.min_yuan.setTextColor(color);
                holder.name.setTextColor(color);
                holder.label.setTextColor(color);
                holder.clabel.setColor(Color.parseColor("#e5e5e5"));
                holder.aclabel.setColor(Color.parseColor("#e5e5e5"));
                holder.progress.setVisibility(View.INVISIBLE);
                holder.status_img.setVisibility(View.VISIBLE);
                holder.status_img.setImageResource(R.drawable.icon_status_product_over);
            }else if("3".equals(list.get(position).getStatus())){
                int color = getResources().getColor(R.color.text_label1);
                holder.rate.setTextColor(color);
                holder.term.setTextColor(color);
                holder.min.setTextColor(color);
                holder.day.setTextColor(color);
                holder.min_yuan.setTextColor(color);
                holder.name.setTextColor(color);
                holder.label.setTextColor(color);
                holder.clabel.setColor(Color.parseColor("#e5e5e5"));
                holder.aclabel.setColor(Color.parseColor("#e5e5e5"));
                holder.progress.setVisibility(View.INVISIBLE);
                holder.status_img.setVisibility(View.VISIBLE);
                holder.status_img.setImageResource(R.drawable.icon_status_product_stop);
            }else{
                int color = getResources().getColor(R.color.text_label2);
                holder.rate.setTextColor(getResources().getColor(R.color.rate_color));
                holder.term.setTextColor(color);
                holder.min.setTextColor(color);
                holder.day.setTextColor(color);
                holder.min_yuan.setTextColor(color);
                holder.name.setTextColor(color);
                holder.label.setTextColor(color);
                holder.clabel.setColor(color);
                String labelcolor = "#fd7328";
                if(!"".equals(list.get(position).getSeries_color()))
                    labelcolor = list.get(position).getSeries_color();
                holder.clabel.setColor(Color.parseColor(labelcolor));
                holder.aclabel.setColor(Color.parseColor("#2a96fb"));
                holder.progress.setVisibility(View.VISIBLE);
                holder.status_img.setVisibility(View.INVISIBLE);
                if(!"".equals(list.get(position).getProgress())){
                    try{
                        holder.progress.setProgress(Integer.parseInt(list.get(position).getProgress()));
                    }catch (Exception e){
                        //do nothing
                    }
                }
            }


            return convertView;
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }
    }

    private class ViewHolder {
        private TextView rate;
        private TextView term;
        private TextView day;
        private TextView min;
        private TextView min_yuan;
        private TextView name;
        private TextView label;
        private ImageView status_img;
        private CircleLabel clabel;
        private CircleLabel aclabel;
        private ArcProgress progress;
    }
}
