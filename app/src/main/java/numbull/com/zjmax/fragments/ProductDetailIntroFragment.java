package numbull.com.zjmax.fragments;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import numbull.com.zjmax.R;
import numbull.com.zjmax.utils.ZjTagHandler;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductDetailIntroFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductDetailIntroFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "memo";
    private static final String ARG_PARAM2 = "imgurl";

    // TODO: Rename and change types of parameters
    private String memo;
    private String imgurl;
    private View rootview;
    private TextView info;
    private ImageView img;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param String memo 1.
     * @param String imgurl 2.
     * @return A new instance of fragment ProductDetailIntroFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProductDetailIntroFragment newInstance(String memo, String imgurl) {
        ProductDetailIntroFragment fragment = new ProductDetailIntroFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, memo);
        args.putString(ARG_PARAM2, imgurl);
        fragment.setArguments(args);
        return fragment;
    }

    public ProductDetailIntroFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            memo = getArguments().getString(ARG_PARAM1);
            imgurl = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootview == null){
            rootview = View.inflate(getActivity(), R.layout.fragment_productdetail_intro, null);
            info = (TextView)rootview.findViewById(R.id.fragment_productdetail_intro_info);
            img = (ImageView)rootview.findViewById(R.id.fragment_productdetail_intro_img);

            if(memo != null)
                info.setText(Html.fromHtml(memo, null, new ZjTagHandler()));

            if(imgurl != null){
                DisplayImageOptions options = new DisplayImageOptions.Builder()
                        .showStubImage(R.drawable.bkg_zjmax)
                        .showImageForEmptyUri(R.drawable.bkg_zjmax)
                        .showImageOnFail(R.drawable.bkg_zjmax)
                        .bitmapConfig(Bitmap.Config.RGB_565)
                        .imageScaleType(ImageScaleType.NONE)
                        .build();
                ImageLoader.getInstance().displayImage(imgurl, img, options);
            }


        }

        ViewGroup parent = (ViewGroup)rootview.getParent();
        if(parent != null){
            parent.removeView(rootview);
        }

        return rootview;
    }


}
