package numbull.com.zjmax.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import numbull.com.zjmax.ProductDetailActivity;
import numbull.com.zjmax.R;
import numbull.com.zjmax.adapters.AutoScrollAdapter;
import numbull.com.zjmax.api.ZJClient;
import numbull.com.zjmax.callback.ResponseCallback;
import numbull.com.zjmax.httpcore.RequestParams;
import numbull.com.zjmax.modules.GetRecommodule;
import numbull.com.zjmax.widgets.ArcProgress;
import numbull.com.zjmax.widgets.AutoScrollViewPager;
import numbull.com.zjmax.widgets.CircleLabel;

/**
 * Created by leo on 2015/3/17.
 */
public class RecomFragment extends Fragment{
    private View rootView;
    private AutoScrollViewPager scrollpager;
    private LinearLayout body;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_recom, container, false);
            scrollpager = (AutoScrollViewPager)rootView.findViewById(R.id.fragment_recom_autopager);
            body = (LinearLayout)rootView.findViewById(R.id.fragment_recom_body);

            LoadData();
        }
        ViewGroup parent = (ViewGroup)rootView.getParent();
        if(parent != null){
            parent.removeView(rootView);
        }

        return rootView;
    }

    private void LoadData(){
        Log.i("startload", "recomd");
        RequestParams params = new RequestParams();
        params.put("a", "124");
        ZJClient.GetRecom(new ResponseCallback<GetRecommodule>(){
            private static final String TAG = "ResponseCallback";

            public void onSuccess(GetRecommodule response) {
                Log.i(TAG, "onSuccess with object returned");
                if("0".equals(response.getE())){
                    UpdateUI(response);
                }
            }

            public void onSuccess(List<GetRecommodule> response) {
                Log.i(TAG, "onSuccess with object list returned: " + response.size());
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
            }

            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
                        + throwable.getMessage());
            }
        }, params);
    }

    private void UpdateUI(final GetRecommodule response) {
        if(response.getList().size() > 0){
            for(int j=0;j<response.getList().size();j++){
                View itemview = View.inflate(getActivity(), R.layout.item_productlist, null);
                TextView rate = (TextView)itemview.findViewById(R.id.item_productlist_rate);
                TextView term = (TextView)itemview.findViewById(R.id.item_productlist_term);
                TextView min  = (TextView)itemview.findViewById(R.id.item_productlist_min);
                TextView name = (TextView)itemview.findViewById(R.id.item_productlist_name);
                TextView label = (TextView)itemview.findViewById(R.id.item_productlist_label);
                CircleLabel clabel = (CircleLabel)itemview.findViewById(R.id.item_productlist_circlelabel);
                ArcProgress progress = (ArcProgress)itemview.findViewById(R.id.item_productlist_progress);

                rate.setText(response.getList().get(j).getRate());
                term.setText(response.getList().get(j).getTerm());
                min.setText(Integer.parseInt(response.getList().get(j).getMin()) / 1000000 + "");
                name.setText(response.getList().get(j).getProject());
                label.setText(response.getList().get(j).getSeries());
                String labelcolor = "#fd7328";
                if(!"".equals(response.getList().get(j).getSeries_color()))
                    labelcolor = response.getList().get(j).getSeries_color();
                clabel.setColor(Color.parseColor(labelcolor));
                if(!"".equals(response.getList().get(j).getProgress())){
                    try{
                        progress.setProgress(Integer.parseInt(response.getList().get(j).getProgress()));
                    }catch (Exception e){
                        //do nothing
                    }
                }
                final int poi = j;
                itemview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), ProductDetailActivity.class);
                        intent.putExtra("product_id", response.getList().get(poi).getId());
                        intent.putExtra("product_project", response.getList().get(poi).getProject());
                        intent.putExtra("product_rate", response.getList().get(poi).getRate());
                        intent.putExtra("product_min", response.getList().get(poi).getMin());
                        intent.putExtra("product_term", response.getList().get(poi).getTerm());
                        intent.putExtra("product_max", response.getList().get(poi).getAvailable());
                        intent.putExtra("product_progress", response.getList().get(poi).getProgress());
                        intent.putExtra("product_remain", response.getList().get(poi).getRemain());
                        intent.putExtra("product_img", response.getList().get(poi).getImg());
                        getActivity().startActivity(intent);
                    }
                });
                body.addView(itemview);
            }
        }

        if (response.getImage().size() > 0) {
            List<String> imgurls = new ArrayList<String>();
            for (int i = 0; i < response.getImage().size(); i++) {
                imgurls.add(response.getImage().get(i).getImage());
            }
            scrollpager.setAdapter(new AutoScrollAdapter(getActivity(), imgurls));
            scrollpager.startAutoScroll(4000);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onResume() {
        super.onResume();
        if(scrollpager != null)
            scrollpager.startAutoScroll(5000);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(scrollpager != null)
            scrollpager.stopAutoScroll();
    }

    @Override
    public void onStop() {
        super.onStop();

    }
}

