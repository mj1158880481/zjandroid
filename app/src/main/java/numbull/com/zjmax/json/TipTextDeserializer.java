package numbull.com.zjmax.json;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * Deserializer of tip text, convert string array to TipText array.
 * @author leo
 *
 */
public class TipTextDeserializer implements JsonDeserializer<TipText> {

	@Override
	public TipText deserialize(JsonElement arg0, Type arg1,
			JsonDeserializationContext arg2) throws JsonParseException {
		TipText tipText = new TipText();
		tipText.setText(arg0.getAsJsonPrimitive().getAsString());
		return tipText;
	}

}
