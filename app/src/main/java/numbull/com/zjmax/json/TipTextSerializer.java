package numbull.com.zjmax.json;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Serializer of TipText.
 * 
 * @author leo
 * 
 */
public class TipTextSerializer implements JsonSerializer<TipText> {

	@Override
	public JsonElement serialize(TipText arg0, Type arg1,
			JsonSerializationContext arg2) {
		return new JsonPrimitive(arg0.getText());
	}
	
}
