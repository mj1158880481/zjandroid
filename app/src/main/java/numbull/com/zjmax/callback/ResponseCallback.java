package numbull.com.zjmax.callback;

import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

/**
 * The response callback of the client http request. Note that: If the server
 * return a JSONObject, onSuccess(T response) will be called. If the server
 * return a JSONArray, onSuccess(List<T> response) will be called. And
 * onFailure(Throwable error) will be called if the request failed or some other
 * error occurs.
 * 
 * @author leo
 * 
 * @param <T>
 */

public class ResponseCallback<T> {
	private static final String TAG = "ResponseCallback";

	public void onSuccess(T response) {
//		Log.i(TAG, "onSuccess with object returned");
	}

	public void onSuccess(List<T> response) {
//		Log.i(TAG, "onSuccess with object list returned: " + response.size());
	}
	
	public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
		Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
				+ throwable.getMessage());
	}

	public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
		Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
				+ throwable.getMessage());
	}
	
	public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
		Log.i(TAG, "onFailure with status code: " + statusCode + " and error: "
				+ throwable.getMessage());
	}
}
