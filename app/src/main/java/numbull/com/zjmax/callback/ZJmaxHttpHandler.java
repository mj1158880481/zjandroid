package numbull.com.zjmax.callback;

/**
 * Created by leo on 2015/3/16.
 */

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import numbull.com.zjmax.httpcore.JsonHttpResponseHandler;
import numbull.com.zjmax.json.TipText;
import numbull.com.zjmax.json.TipTextDeserializer;
import numbull.com.zjmax.json.TipTextSerializer;


public class ZJmaxHttpHandler<T> extends JsonHttpResponseHandler {
    private ResponseCallback<T> callback;
    private Gson gson;
    private Class<T> entityClass;

    @SuppressWarnings("unchecked")
    public ZJmaxHttpHandler(ResponseCallback<T> callback){
        this.callback = callback;
        this.gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
                .enableComplexMapKeySerialization().serializeNulls()
                .setDateFormat("yyyy-MM-dd").setPrettyPrinting()
                .registerTypeAdapter(TipText.class, new TipTextSerializer())
                .registerTypeAdapter(TipText.class, new TipTextDeserializer())
                .create();
        this.entityClass = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONObject response)  {
        super.onSuccess(statusCode, headers, response);
        try {
            this.callback.onSuccess(parseToType(response));
        } catch (JsonSyntaxException err) {
            this.callback.onFailure(statusCode, headers, err, response);
        }

    }

    protected T parseToType(JSONObject response) throws JsonSyntaxException {
        Log.i("response", response.toString());
        T t = this.gson.fromJson(response.toString(), this.entityClass);
        return t;
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
        super.onSuccess(statusCode, headers, response);
        try {
//            Log.i("", response.toString());
            int length = response.length();
            List<T> result = new ArrayList<T>();
            for (int i = 0; i < length; i++) {
                JSONObject jsonObject = response.getJSONObject(i);
                result.add(parseToType(jsonObject));
            }
            this.callback.onSuccess(result);
        } catch (JSONException e) {
            this.callback.onFailure(statusCode, headers, e, response);
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
        this.callback.onFailure(statusCode, headers, throwable, errorResponse);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
        this.callback.onFailure(statusCode, headers, throwable, errorResponse);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
        this.callback.onFailure(statusCode, headers, responseString , throwable);
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, String responseString) {
        if (statusCode != HttpStatus.SC_NO_CONTENT) {
            final byte[] responseBytes = responseString.getBytes();
            final int status = statusCode;
            final Header[] head = headers;
            Runnable parser = new Runnable() {
                @Override
                public void run() {
                    try {
                        final Object jsonResponse = parseResponse(responseBytes);
                        postRunnable(new Runnable() {
                            @Override
                            public void run() {
                                if (jsonResponse instanceof JSONObject) {
                                    onSuccess(status, head, (JSONObject) jsonResponse);
                                } else if (jsonResponse instanceof JSONArray) {
                                    onSuccess(status, head, (JSONArray) jsonResponse);
                                } else if (jsonResponse instanceof String) {
                                    onFailure(status, head, (String) jsonResponse, new JSONException("Response cannot be parsed as JSON data"));
                                } else {
                                    onFailure(status, head, new JSONException("Unexpected response type " + jsonResponse.getClass().getName()), (JSONObject) null);
                                }

                            }
                        });
                    } catch (final JSONException ex) {
                        postRunnable(new Runnable() {
                            @Override
                            public void run() {
                                onFailure(status, head, ex, (JSONObject) null);
                            }
                        });
                    }
                }
            };
            if (!getUseSynchronousMode()) {
                new Thread(parser).start();
            } else {
                // In synchronous mode everything should be run on one thread
                parser.run();
            }
        } else {
            onSuccess(statusCode, headers, new JSONObject());
        }
    }
}
